<?php

namespace CRA\OAuthServerBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use CRA\OAuthServerBundle\Entity\Client;
use Doctrine\Common\Persistence\ObjectManager;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class ClientManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CRASecurityService
     */
    private $securityService;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(ObjectManager $em, CRASecurityService $securityService)
    {
        $this->em = $em;
        $this->securityService = $securityService;
        $this->repository = $em->getRepository("CRAOAuthServerBundle:Client");
    }

    /**
     * @var Client
     */
    public function createClient(string $privilege, Client $admin) {
        $client = Client::create($privilege, $admin);
        /** @var Client $client */
        $user = $client->getUser();
        switch ($privilege) {
            case 'sysadmin' :
            case 'funcadmin' :
                $user->setFileUserKey("");
                $user->setLogUserKey("");
                break;
            case 'user' :
                $user->setFileUserKey($this->securityService->generateSymmetricKey());
                $user->setLogUserKey($this->securityService->generateSymmetricKey());
                break;
        }
        return $client;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function findClientBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function updateClient(Client $client)
    {
        $this->em->persist($client);
        $this->em->flush();
        $client->setClientId($client->getId().'_'.$client->getRandomId());
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function deleteClient(ClientInterface $client)
    {
        $this->em->remove($client);
        $this->em->flush();
    }
}
