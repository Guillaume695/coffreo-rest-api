<?php

namespace CRA\OAuthServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\CoffreoRestApiBundle\Entity\User;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="oauth2_clients")
 * @ORM\Entity(repositoryClass="CRA\OAuthServerBundle\Repository\ClientRepository")
 */
class Client extends BaseClient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * OAuth2 client ID
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=255, nullable=true)
     * @Groups({"sysadmin", "funcAdmin", "user"})
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    protected $clientId;

    /**
     * OAuth2 client secret
     * @var string
     * @Groups({"sysadmin", "funcAdmin", "user"})
     */
    protected $secret;

    /**
     * @var string
     *
     * @ORM\Column(name="privilege", type="string", length=255)
     * @Groups({"sysadmin", "funcAdmin"})
     */
    private $privilege;

    /**
     * Associated User
     * @var User
     * @Groups({"sysadmin", "funcAdmin"})
     * @ORM\OneToOne(targetEntity="CRA\CoffreoRestApiBundle\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user",referencedColumnName="id",nullable=true, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="CRA\OAuthServerBundle\Entity\AccessToken", mappedBy="client", cascade={"persist", "remove"})
     */
    private $accessTokens;

    /**
     * @ORM\OneToMany(targetEntity="CRA\OAuthServerBundle\Entity\AuthCode", mappedBy="client", cascade={"persist", "remove"})
     */
    private $authCodes;

    /**
     * @ORM\OneToMany(targetEntity="CRA\OAuthServerBundle\Entity\RefreshToken", mappedBy="client", cascade={"persist", "remove"})
     */
    private $refreshTokens;

    /**
     * Create a new Client
     * @return Client
     */
    public function create(string $privilege, Client $admin)
    {
        $client = new Client();
        $client->setRandomId(Random::generateToken());
        $client->setSecret(Random::generateToken());
        $client->setAllowedGrantTypes(array('client_credentials'));
        $client->setPrivilege($privilege);
        $user = new User($admin->getClientId());
        $client->setUser($user);
        return $client;
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Check privilege
     *
     * @return Boolean
     */
    public function hasRole(String $role)
    {
        return ($this->getPrivilege() == $role);
    }

    /**
     * Check if the current client has the simpleUser privilege
     *
     * @return Boolean
     */
    public function isSimpleUser()
    {
        return $this->hasRole("user");
    }

    /**
     * Check if the current client has the functionAdmin privilege
     *
     * @return Boolean
     */
    public function isFunctionnalAdmin()
    {
        return $this->hasRole("funcadmin");
    }

    /**
     * Check if the current client has the systemAdmin privilege
     *
     * @return Boolean
     */
    public function isSystemAdmin()
    {
        return $this->hasRole("sysadmin");
    }

    /**
     * Set user
     *
     * @param \CRA\CoffreoRestApiBundle\Entity\User $user
     *
     * @return Client
     */
    public function setUser(\CRA\CoffreoRestApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CRA\CoffreoRestApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return Client
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Check if the client Id is a correct OAuth2 client Id
     * @Assert\IsTrue(message = "Incorrect client_id")
     * @return bool
     */
    public function isClientIdCorrect() {
        $splittedClientId = explode('_', $this->getClientId());
        return ($splittedClientId[0] > 0 && count($splittedClientId) === 2);
    }

    /**
     * Set privilege
     *
     * @param string $privilege
     *
     * @return Client
     */
    public function setPrivilege($privilege)
    {
        $this->privilege = $privilege;

        return $this;
    }

    /**
     * Get privilege
     *
     * @return string
     */
    public function getPrivilege()
    {
        return $this->privilege;
    }
}
