<?php
// src/CRA/CoffreoRestApiBundle/FileService/CRAFuncAdminService.php

namespace CRA\CoffreoRestApiBundle\FuncAdminService;

use Doctrine\ORM\EntityManager;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\File;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\CoffreoRestApiBundle\LogService\CRALogService;
use CRA\CoffreoRestApiBundle\Repository\FileRepository;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class CRAFuncAdminService {

    private $securityService;
    private $logService;
    private $fileService;
    private $fileRepository;
    private $em;

    public function __construct(CRASecurityService $securityService,
                                CRALogService $logService,
                                CRAFileService $fileService,
                                FileRepository $fileRepository,
                                EntityManager $em)
    {
        $this->securityService = $securityService;
        $this->logService = $logService;
        $this->fileService = $fileService;
        $this->fileRepository = $fileRepository;
        $this->em = $em;
    }

    /**
     * Execute the keyRotation on a client
     */
    public function keyRotation(Client $client) {
        // 1. Check if client is a simple user
        if (!$client->isSimpleUser()) {
            throw new BadRequestException("Invalid client_id received.");
        }
        // 3. Set available on true to lock user requests
        $user = $client->getUser();
        $user->setAvailable(false);
        $this->em->flush();
        // 4. Generation of a new set of keys
        $newFileUserKey = $this->securityService->generateSymmetricKeyBIN();
        $newLogUserKey = $this->securityService->generateSymmetricKeyBIN();
        // 5. Duplicate logs, files and mds and create new file entities for each file
        $dataModelToDelete = $this->duplicateAndCreateNewFiles($client, $newFileUserKey, $newLogUserKey);
        // 6. Update user keys
        $user->setAvailable(true);
        $user->setLastKeyRotation(new \DateTime());
        $user->setLogUserKey(sodium_bin2hex($newLogUserKey));
        $user->setFileUserKey(sodium_bin2hex($newFileUserKey));
        $this->em->flush();
        // 7. Delete old files, md, logs and database entries
        foreach ($dataModelToDelete as $oldData) {
            $this->fileService->deleteVersion($oldData);
            $this->logService->deleteLogs($oldData);
            $this->fileRepository->deleteFileByNonce($oldData);
        }
    }

    /**
     * Duplicate all functional logs, files and md of a user, encrypt the copy using the new keys and generate new File entities describing the duplication
     * @return array
     */
    public function duplicateAndCreateNewFiles(Client $client, string $newFileUserKey, string $newLogUserKey)
    {
        $dataModelToDelete = [];
        // 1. Get last version File entity for each file
        $files = $this->fileRepository->getAllLastVersionFiles($client->getUser());
        // 2. For each file, call keyRotation of logService
        foreach($files as $file) {
            // 3. Create datamodel for the current file
            $oldData = new DataModel($client);
            $oldData->updateDataWithFile($file);
            // 4. Update $oldData with last log
            $this->logService->updateDataWithLastLog($oldData);
            // 4.1 Add key rotation log
            $this->logService->appendLog($oldData, "Key rotation");
            // 5. newData will be used to keep track of the logs & data encrypted with the new keys
            $newData = DataModel::duplicateDataModel($oldData);
            // 6. generate a new nonce and reset version to 1 in new data
            $newData->setNonce($this->securityService->generateNonce());
            $newData->setVersion(0);
            // 7. Decrypt functionnal logs with the old keys, duplicate them and encrypt them using the new logUserKey. FileService will be called when needed to execute the key rotation on the files & md.
            $this->logService->keyRotation($oldData, $newData, $newFileUserKey, $newLogUserKey);
            // 8. Create new file. This file will be persisted on next flush as it is added to user's list of files
            $newFile = new File($newData);
            // 8. Append oldData to $dataModelToDelete. Files and mds pointed by the oldData should be deleted at the end of the keyRotation if everything worked normally
            $dataModelToDelete []= $oldData;
        }
        return $dataModelToDelete;
    }

    /**
     * Delete all files, mds and logs of a user
     */
    public function deleteUser(Client $client) {
        // 1. Delete file & md folder
        $this->fileService->deleteUser($client);
        // 2. Delete logs folder
        $this->logService->deleteUser($client);
    }
}
