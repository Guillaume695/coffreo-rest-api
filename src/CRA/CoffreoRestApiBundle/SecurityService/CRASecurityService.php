<?php
// src/CRA/CoffreoRestApiBundle/SecurityService/CRASecurityService.php

namespace CRA\CoffreoRestApiBundle\SecurityService;

use CRA\OAuthServerBundle\Entity\Client;
use FOS\OAuthServerBundle\Model\TokenManager;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CRASecurityService {

	// Length of random ids to generate
	const RANDOM_ID_LENGTH = 10;

	private $oauth_token_manager;
	private $security_token_storage;
	private $hash_algorithm;
	private $symmetric_algorithm;
	
	// Associated Data for AEAD algorithms
	private $associated_data;
	// Keypair for asymmetric encryption
	private $api_asymmetric_key_pair;
	// Nonce for asymmetric encryption
	private $api_asymmetric_nonce;
	// Keypair for signature
	private $api_sign_key_pair;
	
	public function __construct(TokenManager $oauth_token_manager,
								TokenStorage $security_token_storage,
								$hash_algorithm,
								$symmetric_algorithm,
								$associated_data,
								$api_asymmetric_key_pair,
								$api_sign_key_pair,
								$api_asymmetric_nonce)
	{
		$this->oauth_token_manager = $oauth_token_manager;
		$this->security_token_storage = $security_token_storage;
		$this->hash_algorithm = $hash_algorithm;
		$this->symmetric_algorithm = $symmetric_algorithm;
		$this->associated_data = $associated_data;
		$this->api_asymmetric_key_pair = $api_asymmetric_key_pair;
		$this->api_sign_key_pair = $api_sign_key_pair;
		$this->api_asymmetric_nonce = $api_asymmetric_nonce;
	}

	/**
	 * Get hash_algorithm of the current instance
	 * @return string
	 */
	public function getHashAlgorithm() {
		return $this->hash_algorithm;
	}

	/**
	 * Set hash_algorithm for the current instance of CRASecurityService, override parameters.yml
	 */
	public function setHashAlgorithm(string $hashAlgorithm) {
		$this->hash_algorithm = $hashAlgorithm;
	}

	/**
	 * Get symmetric_algorithm of the current instance
	 * @return string
	 */
	public function getSymmetricAlgorithm()
	{
		return $this->symmetric_algorithm;
	}

	/**
	 * Set symmetric_algorithm for the current instance of CRASecurityService, override parameters.yml
	 */
	public function setSymmetricAlgorithm(string $symmetricAlgorithm) {
		$this->symmetric_algorithm = $symmetricAlgorithm;
	}

	/**
	 * Set associated_data for the current instance of CRASecurityService, override parameters.yml
	 */
	public function setAssociatedData(string $associatedData) {
		$this->associated_data = $associatedData;
	}

	/**
	 * Set api_asymmetric_keypair for the current instance of CRASecurityService, override parameters.yml
	 */
	public function setApiAsymmetricKeypair(string $keypair) {
		$this->api_asymmetric_key_pair = $keypair;
	}

	/**
	 * Set api_sign_keypair for the current instance of CRASecurityService, override parameters.yml
	 */
	public function setApiSignKeypair(string $keypair)
	{
		$this->api_sign_key_pair = $keypair;
	}

	/**
	 * Erase the contents of a api_asymmetric_key_pair and api_sign_key_pair as recommended by Libsodium
	 */
	public function wipeApiKeys() {
		sodium_memzero($this->api_asymmetric_key_pair);
		sodium_memzero($this->api_sign_key_pair);
		sodium_memzero($this->api_asymmetric_nonce);
		sodium_memzero($this->associated_data);
	}

	/**
	 * Get client from oauth2 access token
	 * @return Client
	 */
	public function getClient() {
		$token = $this->security_token_storage->getToken();
		$accessToken = $this->oauth_token_manager->findTokenByToken($token->getToken());
		return $accessToken->getClient();
	}

	/**
	 * Compute the hash of $data, using instance's hash_algorithm
	 * @return string
	 */
	public function generateHash(string $data, string $key = null) {
		switch($this->hash_algorithm) {
			case 'libsodium_default':
			case 'blake2b':
			default:
				return sodium_bin2hex(sodium_crypto_generichash($data));
			case 'sha256':
				return hash("sha256", $data);
			case 'sha512':
				return hash("sha512", $data);
			case 'keyed_blake2b':
				return sodium_bin2hex(sodium_crypto_generichash($data, $key));
		}
	}

	/**
	 * Compute the hash of $data, then encrypt this hash with asymmetric encryption
	 * @return string
	 */
	public function generateEncryptedHash(string $data) {
		return $this->asymmetricEncrypt($this->generateHash($data));
	}

	/**
	 * Generate Symmetric key for symmetric_algorithm
	 * @return string
	 */
	public function generateSymmetricKey() {
		switch($this->symmetric_algorithm) {
			case 'libsodium_default' :
			default :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES));
			case 'aes256-GCM' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_AES256GCM_KEYBYTES));
			case 'chacha20-Poly1305' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_KEYBYTES));
			case 'xchacha20-Poly1305' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_KEYBYTES));
		}
	}

	/**
	 * Generate Symmetric key for symmetric_algorithm
	 * @return string
	 */
	public function generateSymmetricKeyBIN()
	{
		switch ($this->symmetric_algorithm) {
			case 'libsodium_default' :
			default :
				return random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);
			case 'aes256-GCM' :
				return random_bytes(SODIUM_CRYPTO_AEAD_AES256GCM_KEYBYTES);
			case 'chacha20-Poly1305' :
				return random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_KEYBYTES);
			case 'xchacha20-Poly1305' :
				return random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_KEYBYTES);
		}
	}

	/**
	 * Generate nonce for symmetric_algorithm
	 * @return string
	 */
	public function generateNonce()
	{
		switch ($this->symmetric_algorithm) {
			case 'libsodium_default' :
			default :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES));
			case 'aes256-GCM' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_AES256GCM_NPUBBYTES));
			case 'chacha20-Poly1305' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES));
			case 'xchacha20-Poly1305' :
				return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES));
		}
	}

	/**
	 * Generate random fileID
	 * @return string
	 */
	public function generateRandomId() {
		return sodium_bin2hex(random_bytes(CRASecurityService::RANDOM_ID_LENGTH));
	}

	/**
	 * Encrypt $data, using instance's secret_key_algorithm
	 * @return string
	 */
	public function symmetricEncrypt(string $data, string $nonce, string $key) {
		switch($this->symmetric_algorithm) {
			case 'libsodium_default':
			default:
				return sodium_bin2hex(sodium_crypto_secretbox($data, $nonce, $key));
			case 'aes256-GCM':
				if (sodium_crypto_aead_aes256gcm_is_available()) {
					return sodium_bin2hex(sodium_crypto_aead_aes256gcm_encrypt($data, $this->associated_data, $nonce, $key));
				} else {
					return sodium_bin2hex(sodium_crypto_secretbox($data, $nonce, $key));
				}
			case 'chacha20-Poly1305':
				return sodium_bin2hex(sodium_crypto_aead_chacha20poly1305_encrypt($data, $this->associated_data, $nonce, $key));
			case 'xchacha20-Poly1305':
				return sodium_bin2hex(sodium_crypto_aead_xchacha20poly1305_ietf_encrypt($data, $this->associated_data, $nonce, $key));
		}
	}

	/**
	 * Encrypt $data, using instance's secret_key_algorithm (BIN)
	 * @return string
	 */
	public function symmetricEncryptBIN(string $data, string $nonce, string $key)
	{
		switch ($this->symmetric_algorithm) {
			case 'libsodium_default' :
			default :
				return sodium_crypto_secretbox($data, $nonce, $key);
			case 'aes256-GCM' :
				if (sodium_crypto_aead_aes256gcm_is_available()) {
					return sodium_crypto_aead_aes256gcm_encrypt($data, $this->associated_data, $nonce, $key);
				}
				else {
					return sodium_crypto_secretbox($data, $nonce, $key);
				}
			case 'chacha20-Poly1305' :
				return sodium_crypto_aead_chacha20poly1305_encrypt($data, $this->associated_data, $nonce, $key);
			case 'xchacha20-Poly1305' :
				return sodium_crypto_aead_xchacha20poly1305_ietf_encrypt($data, $this->associated_data, $nonce, $key);
		}
	}

	/**
	 * Decrypt $data, using instance's symmetric_algorithm
	 * @return string|bool
	 */
	public function symmetricDecrypt(string $data, string $nonce, string $key)
	{
		switch ($this->symmetric_algorithm) {
			case 'libsodium_default':
			default:
				return sodium_crypto_secretbox_open($data, $nonce, $key);
			case 'aes256-GCM':
				if (sodium_crypto_aead_aes256gcm_is_available()) {
					return sodium_crypto_aead_aes256gcm_decrypt($data, $this->associated_data, $nonce, $key);
				} else {
					return sodium_crypto_secretbox_open($data, $nonce, $key);
				}
			case 'chacha20-Poly1305':
				return sodium_crypto_aead_chacha20poly1305_decrypt($data, $this->associated_data, $nonce, $key);
			case 'xchacha20-Poly1305':
				return sodium_crypto_aead_xchacha20poly1305_ietf_decrypt($data, $this->associated_data, $nonce, $key);
		}
	}

	/**
	 * Encrypt $data, using api secret key
	 * @return string
	 */
	public function asymmetricEncrypt(string $data)
	{
		return sodium_bin2hex(sodium_crypto_box($data, sodium_hex2bin($this->api_asymmetric_nonce), sodium_hex2bin($this->api_asymmetric_key_pair)));
	}

	/**
	 * Encrypt $data, using api secret key
	 * @return string
	 */
	public function asymmetricEncryptBIN(string $data)
	{
		return sodium_crypto_box($data, sodium_hex2bin($this->api_asymmetric_nonce), sodium_hex2bin($this->api_asymmetric_key_pair));
	}

	/**
	 * Decrypt $data, using api public key
	 * @return string|bool
	 */
	public function asymmetricDecrypt(string $data)
	{
		return sodium_crypto_box_open($data, sodium_hex2bin($this->api_asymmetric_nonce), sodium_hex2bin($this->api_asymmetric_key_pair));
	}

	/**
	 * Sign $data, using api secret key
	 * @return string
	 */
	public function sign(string $data)
	{
		return sodium_crypto_sign($data, sodium_crypto_sign_secretkey(sodium_hex2bin($this->api_sign_key_pair)));
	}

	/**
	 * Check signature of $data and return $data without signature, using api public key
	 * @return string|bool
	 */
	public function sign_open(string $data)
	{
		return sodium_crypto_sign_open($data, sodium_crypto_sign_publickey(sodium_hex2bin($this->api_sign_key_pair)));
	}
}