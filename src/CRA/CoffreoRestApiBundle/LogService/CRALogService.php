<?php
// src/CRA/CoffreoRestApiBundle/LogService/CRALogService.php

namespace CRA\CoffreoRestApiBundle\LogService;

use JMS\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Asset\Packages;
use JMS\Serializer\SerializationContext;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Entity\File;
use CRA\CoffreoRestApiBundle\Model\LogModel;
use Symfony\Component\Filesystem\Filesystem;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\CoffreoRestApiBundle\Model\ProofModel;
use CRA\CoffreoRestApiBundle\Model\LastLogModel;
use CRA\CoffreoRestApiBundle\Model\StorageLogModel;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Filesystem\Exception\IOException;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use CRA\CoffreoRestApiBundle\Exception\SignatureFailureException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRALogService {

    private $securityService;
    private $fileService;
    private $func_log_path;
    private $target_dir_file;
    private $assets;
    private $hash_algorithm;
    private $symmetric_algorithm;
    private $jmsSerializer;

    public function __construct(CRASecurityService $securityService,
                                CRAFileService $fileService,
                                $func_log_path,
                                $target_dir_file,
                                Packages $assets,
                                $hash_algorithm,
                                $symmetric_algorithm,
                                Serializer $jmsSerializer)
    {
        $this->fileService = $fileService;
        $this->securityService = $securityService;
        $this->func_log_path = $func_log_path;
        $this->target_dir_file = $target_dir_file;
        $this->assets = $assets;
        $this->hash_algorithm = $hash_algorithm;
        $this->symmetric_algorithm = $symmetric_algorithm;
        $this->jmsSerializer = $jmsSerializer;
    }

    /**
     * Get log directory
     */
    public function getFuncLogPath() {
        return $this->func_log_path;
    }

    /**
     * Append new update log
     */
    public function appendUpdateLog(DataModel $data)
    {
        switch ($data->getUpdateType()) {
            case 'md':
                $this->appendLog($data, "Update (md)");
                break;
            case 'doc':
                // Increment version nb
                $data->incrementVersion();
                $this->createNewStorage($data);
                $this->appendLog($data, "New storage (file update)");
                $this->appendLog($data, "Update (doc)");
                break;
            default:
                // Increment version nb
                $data->incrementVersion();
                $this->createNewStorage($data);
                $this->appendLog($data, "New storage (file update)");
                $this->appendLog($data, "Update (doc & md)");
                break;
        }
    }

    /**
     * Append new download log
     */
    public function appendDownloadLog(DataModel $data, int $downloadedVersion)
    {
        switch ($data->getDownloadType()) {
            case 'md' :
                $this->appendLog($data, "Download (md) - version " . strval($downloadedVersion));
                // For clean response purposes
                $data->resetInitFileName();
                break;
            case 'doc' :
                $this->appendLog($data, "Download (doc) - version " . strval($downloadedVersion));
                // For clean response purposes
                $data->resetInitMdName();
                break;
            default :
                $this->appendLog($data, "Download (doc & md) - version " . strval($downloadedVersion));
                break;
        }
    }

    /**
     * Append new log to the linked lists of functionnal logs of the file described by $data
     */
    public function appendLog(DataModel $data, string $action)
    {
        // 1. Generate file & md hashes
        $this->generateHashes($data);
        // 2. Create the new log instance
        $log = new LogModel($data, $action);

        // 3. For new uploads, we need to create last log and storage files
        if ($data->getLastLogName() == "") {
            // 4. Create storage log & last log json file
            $this->createLogFiles($data);
        }
        // 5. Write the new log at the end of the log chain
        $this->chainLastLog($data, $log);
    }

    /**
     * Chain $log to the linked log list at $fullLastLogPath
     */
    public function chainLastLog(DataModel $data, LogModel $log)
    {
        // 1. Read and decrypt last log file
        $lastLog = $this->readAndDecryptLastLog($data);
        // 2. Last raw entry can be empty (it happens after a call to updateDataWithLastLog for instance)
        if ($lastLog->getLastEntry()) {
            // 2.1 Check signed proof and insert last log entry into storage
            $this->checkSignatureAndStoreLastEntry($lastLog);
        }
        // 5. Update prevSignedProof of new log with signedProof of last log
        $log->setPrevSignedProof($lastLog->getSignedProof());
        
        // 6. Encrypt hash of new log and save it in signed_proof of $lastLogJsonFile
        $rawNewLog = $this->serializeLog($log);
        $lastLog->setSignedProof($this->securityService->generateEncryptedHash($rawNewLog));

        // 7. Encrypt new log and update last-log-[fileId]-[randomId].json
        $lastLog->setLastRawEntry($rawNewLog);
        $this->encryptAndWriteLastLog($data, $lastLog);
    }

    /**
     * Update DataModal $data with last log data
     */
    public function updateDataWithLastLog(DataModel $data)
    {
        $lastEntry = null;
        // 1. If $data has a path to a last log json file, read and try to decrypt last raw entry
        if (is_file($this->func_log_path . $data->getFullLastLogPath())) {
            // 1.1 Read and parse last log json file
            $lastLog = $this->readAndDecryptLastLog($data);
            $lastEntry = $lastLog->getLastEntry();
            // 1.2 If last raw entry isn't empty, check signature and push last raw entry to storage
            if ($lastEntry) {
                $this->checkSignatureAndStoreLastEntry($lastLog);
                $lastLog->setLastRawEntry("");
                $this->encryptAndWriteLastLog($data, $lastLog);
                // update cnil value of data
                $data->setCnil($lastLog->getCnil());
            }
        }
        // 2. If last raw entry was empty (or if $data doesn't have a path to a last log json file), read last log from storage log
        if (!$lastEntry) {
            // 2.1 Update hash algorithm and symmetric algorithm from storage log's header
            $header = $this->readHeaderFromStorage($data->getFullStorageLogPath());
            $this->hash_algorithm = $header['hash_algorithm'];
            $this->symmetric_algorithm = $header['symmetric_algorithm'];
            $data->setCnil($header['cnil']);
            // 2.2 Read and decrypt storage log last entry
            $lastEntry = $this->readAndDecryptStorageLogLastEntry($data);
        }
        // 3. Update dataModel with last log entry
        $data->updateDataWithLastEntry($lastEntry);
    }

    /**
     * Create a new storage-log-[fileId]-[randomId].json file
     */
    public function createNewStorage(DataModel $data)
    {
        // 1. Read and decrypt last log
        $lastLog = $this->readAndDecryptLastLog($data);
        // 2. If last entry wasn't empty, check signature and store it to current storage file 
        if ($lastLog->getLastEntry()) {
            $this->checkSignatureAndStoreLastEntry($lastLog);
        }
        // 3. Create new StorageLogModel instance
        $newStorageLog = $this->createNewStorageLog($data);
        $newStorageFullPath = $data->getLastLogDir() . 'storage/storage-log-' . $data->getFileId() . '-' . $this->securityService->generateRandomId() . '.json';
        // 5. Link old and new storage jsons
        $newStorageLog->setPreviousStorageLog($data->getFullStorageLogPath());
        // 6. Update storage log path in DataModel
        $data->setFullStorageLogPath($newStorageFullPath);
        // 7. Write new storage log json file
        $this->writeStorageLog($data, $newStorageLog);
        // 8. Empty last entry of last log JSON and update current_storage_json
        $lastLog->setLastRawEntry("");
        $lastLog->setCurrentStorageLog($newStorageFullPath);
        // 9. Update last log JSON file
        $this->writeLastLog($data, $lastLog);
    }

    /**
     * Generates functional logs history and get final proof for a file
     */
    public function getProof(DataModel $data, ProofModel $proofModel)
    {
        // 1. Read last log
        $lastLog = $this->readAndDecryptLastLog($data);
        // 2. Get storage log paths list
        $storageLogPaths = $this->getStorageLogPathsFromLastLog($lastLog);
        $signedProof = '';
        $proofModel->setValid(true);
        // 3. Start proof at the beginning of the file history
        foreach($storageLogPaths as $storageLogTemp) {
            // 4. Check integrity of the current storage file
            $signedProof = $this->checkStorageLogIntegrity($data, $storageLogTemp, $signedProof, $proofModel);
            // Leave if proof failed
            if (!$proofModel->isValid()) {
                return;
            }
        }
        // 5. Check if the last signedProof is the same as the last log signed proof
        $proofModel->setValid(($signedProof == $lastLog->getSignedProof()));
        $proofModel->setProof($lastLog->getSignedProof());
        // 6. Append log for proof generation
        $this->appendLog($data, "Proof generation");
    }

    /**
     * Check integrity of a storage log, given a first signed proof, append log entris to proofModel, return the signed hash of the last log entry.
     * @return string
     */
    public function checkStorageLogIntegrity(DataModel $data,
                                            string $storageLogPath,
                                            string $firstSignedProof,
                                            ProofModel $proofModel)
    {
        $storageHistory = [];
        // 1. Read storage file entirely
        $storageLog = $this->readStorageLog($storageLogPath);
        // 2. Check signed proof for each log entry
        $previousSignedProof = $firstSignedProof;
        foreach($storageLog->getRawEntries() as $rawEntry) {
            // 3. Decrypt entry
            $rawLog = $this->decryptRawLog($data, $rawEntry);
            $log = $this->deserializeToLogModel($rawLog);
            // 4. Check previous signed proof
            if($log->getPrevSignedProof() != $previousSignedProof) {
                $proofModel->setValid(false);
                $proofModel->appendStorageHistory($storageLogPath, $storageHistory, $storageLog);
                $proofModel->setProof($log->getPrevSignedProof());
                return $log->getPrevSignedProof();
            }
            // 5. Append log to storage history
            $storageHistory []= $log;
            // 6. Update previousSignedProof
            $previousSignedProof = $this->securityService->generateEncryptedHash($rawLog);
        }
        // 7. Add storage history to proofModel
        $proofModel->appendStorageHistory($storageLogPath, $storageHistory, $storageLog);
        return $previousSignedProof;
    }

    /**
     * Update Cnil state of last log
     */
    public function updateCnilState(DataModel $data) {
        // 1. Read and decrypt last log
        $lastLog = $this->readAndDecryptLastLog($data);
        // 2. Update Cnil state
        $lastLog->setCnil($data->getCnil());
        // 3. Write last log
        $this->writeLastLog($data, $lastLog);
    }

    /**
     * Duplicate and encrypt logs with a new key, execute a key rotation
     */
    public function keyRotation(DataModel $oldData, DataModel $newData, string $newFileUserKey, string $newLogUserKey)
    {
        // During this function, we will have to call fileKeyRotation and mdKeyRotation of CRAFileService in order to execute key rotation on files. We will keep track of each fileName and mdName so that we can detect new versions and therefore know when to call those functions.
        $previousFullFilePath = '';
        $previousFullMdPath = '';

        // 1. Read and decrypt last log
        $oldLastLog = $this->readAndDecryptLastLog($oldData);
        // 1.1 If oldLastLog has a raw log entry, store it to storage
        if ($oldLastLog->getLastEntry()) {
            $this->checkSignatureAndStoreLastEntry($oldLastLog);
        }
        // 2. Get storage logs list
        $storageLogPaths = $this->getStorageLogPathsFromLastLog($oldLastLog);
        // 4. Browse all log entries starting by the first one
        $lastSignedProof = '';
        $previousFullStorageLogPath = '';
        foreach($storageLogPaths as $storageLogPath) {
            $newData->incrementVersion();
            // 5. Read storage log
            $storageLog = $this->readStorageLog($storageLogPath);
            // 6. Create new storage log
            $randomId = $this->securityService->generateRandomId();
            $newData->setStorageLogName('storage-log-' . $newData->getFileId() . '-' . $randomId . '.json');
            $newStorageLogTemp = $this->createNewStorageLog($newData);
            $newStorageLogTemp->setPreviousStorageLog($previousFullFilePath);
            $previousFullFilePath = $newData->getFullStorageLogPath();
            $this->writeStorageLog($newData, $newStorageLogTemp);
            // 6. Duplicate each log Entry in the new storage
            foreach($storageLog->getRawEntries() as $logEntry) {
                $decryptedLogEntry = $this->decryptRawLog($oldData, $logEntry);
                if ($decryptedLogEntry === false) {
                    throw new AccessDeniedException();
                }
                $lastSignedProof = $this->storageLogEntryKeyRotation($this->deserializeToLogModel($decryptedLogEntry), $oldData, $newData, $newFileUserKey, $newLogUserKey, $lastSignedProof, $previousFullFilePath, $previousFullMdPath);
            }
            if ($oldData->getVersion() > $newData->getVersion()) {
                // 7. Create new File entry for the new storage file. This entity will automatically be persisted at the next flush in the controller that called keyRotation
                $newFile = new File($newData);
                $newFile->setFullLastLogPath("");
            }
        }
        // 7. Duplicate last log
        $randomId = $this->securityService->generateRandomId();
        $newData->setLastLogName('last-log-' . $newData->getFileId() . '-' . $randomId . '.json');
        $newLastLog = $this->createNewLastLog($newData);
        $newLastLog->setSignedProof($lastSignedProof);
        $this->writeLastLog($newData, $newLastLog);
    }

    /**
     * Duplicate $logEntry into storage of $newData during a keyRotation
     * @return string $lastSignedProof
     */
    public function storageLogEntryKeyRotation(LogModel $logEntry,
                                                DataModel $oldData,
                                                DataModel $newData,
                                                string $newFileUserKey,
                                                string $newLogUserKey,
                                                string $lastSignedProof,
                                                string $previousFullFilePath,
                                                string $previousFullMdPath)
    {
        // 1. Call fileKeyRotation and mdKeyRotation if there is a new version of the file or md
        if ($previousFullFilePath != $logEntry->getFullFilePath()) {
            $this->fileService->fileKeyRotation($oldData, $newData, $newFileUserKey);
            $newData->setFileHash($oldData->getPreviousFileHash());
        }
        if ($previousFullMdPath != $logEntry->getFullMdPath()) {
            $this->fileService->mdKeyRotation($oldData, $newData, $newFileUserKey);
            $newData->setMdHash($oldData->getPreviousMdHash());
        }
        // 2. Create new log entry
        $newLogEntry = new LogModel($newData, $logEntry->getAction());
        // 3. Set previous signed proof
        $newLogEntry->setPrevSignedProof($lastSignedProof);
        // 4. Generate signed proof and put newLogEntry in storage
        $newRawLogEntry = $this->serializeLog($newLogEntry);
        $encryptedNewRawLog = $this->encryptRawLog($newData, $newRawLogEntry, $newLogUserKey);
        $this->writeLogEntryInStorage($newData->getFullStorageLogPath(), $encryptedNewRawLog);
        return $this->securityService->generateEncryptedHash($newRawLogEntry);
    }

    /**
     * Generate storage log paths from last log. The first element in the answer is the most recent storage log.
     */
    public function getStorageLogPathsFromLastLog(LastLogModel $lastLog) {
        $storageLogPaths = [];
        // 1. Fetch the first storage log path
        $storageLogTemp = $lastLog->getCurrentStorageLog();
        while($storageLogTemp !== '') {
            // 2. Append storage path to the storageLogPaths array
            array_unshift($storageLogPaths, $storageLogTemp);
            // 3. Get header of storage log
            $header = $this->readHeaderFromStorage($storageLogTemp);
            // 4. Get the next storageLogTemp
            $storageLogTemp = $header['previous_storage_log'];
        }
        return $storageLogPaths;
    }

    /**
     * Check signed proof of last entry in last log, then store last entry in storage log
     */
    public function checkSignatureAndStoreLastEntry(LastLogModel $lastLog) {
        // 1. Check SignedProof from last log
        if (!$this->checkLastLogSignature($lastLog)) {
            throw new AccessDeniedException("Signature failure");
        }
        // 2. Insert last log into storage
        $this->writeLogEntryInStorage($lastLog->getCurrentStorageLog(), $lastLog->getLastRawEntry());
    }

    /**
     * Check signature of a log
     */
    public function checkLogSignature(LogModel $log, string $signedProof) {
        return (sodium_memcmp($signedProof, $this->securityService->generateEncryptedHash($this->serializeLog($log))) == 0);
    }

    /**
     * Check signature of the last log entry in the last log json file
     */
    public function checkLastLogSignature(LastLogModel $lastLog)
    {
        return $this->checkLogSignature($lastLog->getLastEntry(), $lastLog->getSignedProof());
    }

    /**
     * Generate hashes for file and metadata
     */
    public function generateHashes(DataModel $data)
    {
        // File
        if ($data->getFileRawData()) {
            // If dataModel has file raw data, generate hash from it
            $data->setFileHash($this->securityService->generateHash($data->getFileRawData()));
        } else {
            // If not, get hash of previous log entry
            $data->setFileHash($data->getPreviousFileHash());
        }
        // If file hash is still empty, read file and re-calculate hash
        if (!$data->getFileHash()) {
            $this->fileService->readAndDecryptFile64($data);
            $data->setFileHash($this->securityService->generateHash($data->getFileRawData()));
        }
        // Metadatas
        if ($data->getMdRawData()) {
            // If DataModel has md raw data, generate hash from it
            $data->setMdHash($this->securityService->generateHash($data->getMdRawData()));
        } else {
            // If not, get hash of previous log entry
            $data->setMdHash($data->getPreviousMdHash());
        }
        // If md hash is still empty, read md and re-calculate hash
        if (!$data->getMdHash()) {
            $this->fileService->readAndDecryptMd64($data);
            $data->setMdHash($this->securityService->generateHash($data->getMdRawData()));
        }
    }

    /**
     * Create and write new empty last log and storage log json files
     */
    public function createLogFiles(DataModel $data)
    {
        // A random id is generated for the new file
        $randomId = $this->securityService->generateRandomId();
        $data->setLastLogName('last-log-' . $data->getFileId() . '-' . $randomId . '.json');
        $data->setStorageLogName('storage-log-' . $data->getFileId() . '-' . $randomId . '.json');
        // Creation of a new last log instance
        $lastLog = $this->createNewLastLog($data);
        // Creation of a new storage log instance
        $storageLog = $this->createNewStorageLog($data);

        // Write new last log
        $this->writeLastLog($data, $lastLog);
        // Write new storage log
        $this->writeStorageLog($data, $storageLog);
    }

    /**
     * Create an empty last log
     * @return LastLogModel
     */
    public function createNewLastLog(DataModel $data)
    {
        $lastLog = LastLogModel::createNewLastLog($data);
        $lastLog->setModel($this->assets->getUrl('model/last-log-model.json'));
        $lastLog->setHashAlgorithm($this->hash_algorithm);
        $lastLog->setSymmetricAlgorithm($this->symmetric_algorithm);
        return $lastLog;
    }

    /**
     * Create an empty storage log
     * @return StorageLogModel
     */
    public function createNewStorageLog(DataModel $data)
    {
        $storageLog = StorageLogModel::createNewStorageLog($data);
        $storageLog->setModel($this->assets->getUrl('model/storage-log-model.json'));
        $storageLog->setHashAlgorithm($this->hash_algorithm);
        $storageLog->setSymmetricAlgorithm($this->symmetric_algorithm);
        return $storageLog;
    }

    /**
     * Read last log JSON file and decrypt its last raw entry
     * @return LastLogModel
     */
    public function readAndDecryptLastLog(DataModel $data)
    {
        // 1. Get LastLogModel instance from json file
        $lastLog = $this->readLastLog($data);
        // 2. Decrypt last log entry if not empty
        if ($lastLog->getLastRawEntry() != '') {
            // 3. Decrypt last entry
            $decryptedLastEntry = $this->decryptRawLog($data, $lastLog->getLastRawEntry());
            // 4. Deserialize to LogModel instance
            $lastLog->setLastEntry($this->deserializeToLogModel($decryptedLastEntry));
        }
        return $lastLog;
    }

    /**
     * Read storage log JSON file and decrypt its last raw entry (without reading / parsing the entire file)
     * @return LogModel
     */
    public function readAndDecryptStorageLogLastEntry(DataModel $data)
    {
        $lastEntry = null;
        // 1. Read last raw entry in storage log file
        $lastRawEntry = $this->readLastEntryFromStorage($data->getFullStorageLogPath(), $data);
        // 2. Decrypt last storage log raw entry if not empty
        if ($lastRawEntry != '') {
            // Decrypt last entry
            $decryptedLastEntry = $this->decryptRawLog($data, $lastRawEntry);
            // Deserialize to LogModel instance
            $lastEntry = $this->deserializeToLogModel($decryptedLastEntry);
        }

        return $lastEntry;
    }

    /**
     * Encrypt and write last log json file
     */
    public function encryptAndWriteLastLog(DataModel $data, LastLogModel $lastLog)
    {
        try {
            if($lastLog->getLastRawEntry() !== '') {
                // Encrypt last raw log entry and serialize last log
                $lastLog->setLastRawEntry($this->encryptRawLog($data, $lastLog->getLastRawEntry()));
            }
            $rawLastLog = $this->serializeLog($lastLog);
            // Write last log json file
            file_put_contents($this->func_log_path . $data->getFullLastLogPath(), $rawLastLog, LOCK_EX);
        } catch (IOException $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }

    /**
     * Encrypt a raw log entry
     * @return string|bool
     */
    public function encryptRawLog(DataModel $data, string $encryptRawLog, string $logUserKey = null)
    {
        if (!$logUserKey) {
            $logUserKey = $data->getClient()->getUser()->getLogUserKeyBIN();
        }
        $result = $this->securityService->symmetricEncrypt($encryptRawLog, $data->getNonceBIN(), $logUserKey);
        if ($result === false) {
            throw new AccessDeniedException('Raw log encryption error');
        }
        return $result;
    }

    /**
     * Decrypt a raw log entry
     * @return string|bool
     */
    public function decryptRawLog(DataModel $data, string $log)
    {
        $result = $this->securityService->symmetricDecrypt(sodium_hex2bin($log), $data->getNonceBIN(), $data->getClient()->getUser()->getLogUserKeyBIN());
        if ($result === false) {
            throw new AccessDeniedException('Raw log decryption error');
        }
        return $result;
    }

    /**
     * Delete all logs associated to DataModel input
     */
    public function deleteLogs(DataModel $data)
    {
        try {
            // Only do something from the beginning of the linked list of logs
            if ($data->getLastLogName() != '') {
                // 1. Open last log
                $lastLog = $this->readLastLog($data);
                // 2. Delete last log file
                unlink($this->func_log_path . $data->getFullLastLogPath());
                // 3. Delete all storage log
                $storageLogPathTemp = $lastLog->getCurrentStorageLog();
                while ($storageLogPathTemp != '') {
                    // 4. Parse storage log file
                    $storageLogJsonFile = $this->readStorageLog($storageLogPathTemp);
                    // 5. Delete storage log file
                    unlink($this->func_log_path . $storageLogPathTemp);
                    // 6. Go to previous storage log file
                    $storageLogPathTemp = $storageLogJsonFile->getPreviousStorageLog();
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

     /**
     * Delete last storage from path
     */
    public function deleteStorage(string $storageLogPath) {
        try {
            unlink($this->func_log_path . $storageLogPath);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Delete the directory of the user
     */
    public function deleteUser(Client $client)
    {
        $fs = new Filesystem();
        $userPath = $this->func_log_path . substr($client->getClientId(), 0, CRASecurityService::RANDOM_ID_LENGTH);

        // Delete the folder
        try {
            if (is_dir($userPath)) {
                $fs->remove($userPath);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Read last log json file and deserialize it to a LastLogModel instance
     * @return LastLogModel
     */
    public function readLastLog(DataModel $data) {
        try {
            // Read json file
            $jsonRawLastLog = file_get_contents($this->func_log_path . $data->getFullLastLogPath());
            // Deserialize raw data
            $lastLog = $this->deserializeToLastLogModel($jsonRawLastLog);
            // Update security service settings
            $this->securityService->setHashAlgorithm($lastLog->getHashAlgorithm());
            $this->securityService->setSymmetricAlgorithm($lastLog->getSymmetricAlgorithm());
            return $lastLog;

        } catch(IOException $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }

    /**
     * Read storage log json file and deserialize it to a StorageLogModel instance
     * @return StorageLogModel
     */
    public function readStorageLog(string $fullStorageLogPath)
    {
        try {
            // Read json file
            $jsonRawStorageLog = file_get_contents($this->func_log_path . $fullStorageLogPath);
            // Deserialize raw data
            $storageLog = $this->deserializeToStorageLogModel($jsonRawStorageLog);
            // Update security service settings
            $this->securityService->setHashAlgorithm($storageLog->getHashAlgorithm());
            $this->securityService->setSymmetricAlgorithm($storageLog->getSymmetricAlgorithm());
            return $storageLog;

        } catch (IOException $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }

    /**
     * Read storage log json file and deserialize it to a StorageLogModel instance, using DataModel instance
     * @return StorageLogModel
     */
    public function readStorageLogFromData(DataModel $data)
    {
        return $this->readStorageLog($data->getFullStorageLogPath());
    }

    /**
     * Write last log instance to a json file
     */
    public function writeLastLog(DataModel $data, LastLogModel $lastLog) {
        try {
            $lastLogDir = $this->func_log_path . $data->getLastLogDir();
            if (!is_dir($lastLogDir)) {
                // create folders recursively
                mkdir($lastLogDir, 0755, true);
            }
            // Serialization context
            $context = SerializationContext::create()->setGroups(array('log'));

            // Write json file
            file_put_contents($lastLogDir . $data->getLastLogName(), $this->jmsSerializer->serialize($lastLog, 'json', $context), LOCK_EX);

        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Write storage log instance to a json file
     */
    public function writeStorageLog(DataModel $data, StorageLogModel $storageLog)
    {
        try {
            $storageLogDir = $this->func_log_path . $data->getLastLogDir() . '/storage/';
            if (!is_dir($storageLogDir)) {
                // create folders recursively
                mkdir($storageLogDir, 0755, true);
            }

            // Write json file
            file_put_contents($storageLogDir . $data->getStorageLogName(), $this->serializeLog($storageLog, LOCK_EX));

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Get last log from storage log at $storageLogPath (without reading / parsing the entire file)
     * @return string
     */
    public function readLastEntryFromStorage(string $storageLogPath, DataModel $data = null)
    {
        try {
            // 1. Open storage log
            $file = fopen($this->func_log_path . $storageLogPath, 'r');
            // 2. Seek to the end
            fseek($file, -1, SEEK_END);
            // 3. Find last ',' or '[' (beguinning of the raw_entries)
            $c = fread($file, 1);
            while ($c !== ',' && $c !== '[') {
                fseek($file, -2, SEEK_CUR);
                $c = fread($file, 1);
            }
            $lastLogCipher = "";
            fread($file, 1);
            $c = fread($file, 1);
            // 4. Copy last raw entry
            while ($c !== '"' && $c !== ']') {
                $lastLogCipher = $lastLogCipher . $c;
                $c = fread($file, 1);
            }
            // 5. Close file
            fclose($file);
            return $lastLogCipher;

        } catch (IOException $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }
    
    /**
     * Read header of storage log without reading / parsing the entire file
     * @return string
     */
    public function readHeaderFromStorage(string $storageLogPath) {
        try {
            // 1. Open storage log
            $file = fopen($this->func_log_path . $storageLogPath, 'r');
            // 2. Get the entire header
            fseek($file, 10, SEEK_CUR);
            $rawHeader = '';
            $c = fread($file, 1);
            while ($c != '}') {
                $rawHeader = $rawHeader . $c;
                $c = fread($file, 1);
            }
            $rawHeader = $rawHeader . '}';
            // 3. Parse rawHeader
            $header = json_decode($rawHeader, true);
            // 6. Close file
            fclose($file);
            return $header;
        } catch (IOException $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }

    /**
     * Append $lastRawEntry to storage log at $storageLogPath (without reading / parsing the entire file)
     */
    public function writeLogEntryInStorage(string $storageLogPath, string $lastRawEntry)
    {
        try {
            // Open storage log
            $file = fopen($this->func_log_path . $storageLogPath, 'r+');
            // Seek to the end
            fseek($file, -3, SEEK_END);
            if (fread($file, 1) === '"') {
                // add the trailing comma
                fwrite($file, ',');
            }
            // add the new json string
            fwrite($file, '"' . $lastRawEntry . '"]}');
            fclose($file);
        } catch (Exception $e) {
            throw new AccessDeniedException($e->getMessage());
        }
    }

    /**
     * Serialize to json
     * @return string
     */
    public function serializeLog($log)
    {
        // Serialization context
        $context = SerializationContext::create()->setGroups(array('log'));
        return $this->jmsSerializer->serialize($log, 'json', $context);
    }

    /**
     * Deserialize json to LogModel
     * @return LogModel
     */
    public function deserializeToLogModel(string $rawLogEntry) {
        if ($rawLogEntry === '') {
            return null;
        }
        return $this->jmsSerializer->deserialize($rawLogEntry, 'CRA\CoffreoRestApiBundle\Model\LogModel', 'json');
    }

    /**
     * Deserialize json to LastLogModel
     * @return LastLogModel
     */
    public function deserializeToLastLogModel(string $rawLastLog)
    {
        if ($rawLastLog === '') {
            return null;
        }
        return $this->jmsSerializer->deserialize($rawLastLog, 'CRA\CoffreoRestApiBundle\Model\LastLogModel', 'json');
    }

    /**
     * Deserialize json to StorageLogModel
     * @return StorageLogModel
     */
    public function deserializeToStorageLogModel(string $rawStorageLog)
    {
        if ($rawStorageLog === '') {
            return null;
        }
        return $this->jmsSerializer->deserialize($rawStorageLog, 'CRA\CoffreoRestApiBundle\Model\StorageLogModel', 'json');
    }
}
