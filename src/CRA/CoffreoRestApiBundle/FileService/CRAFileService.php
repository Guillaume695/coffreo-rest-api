<?php
// src/CRA/CoffreoRestApiBundle/FileService/CRAFileService.php

namespace CRA\CoffreoRestApiBundle\FileService;

use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CRAFileService {

    private $securityService;
    private $target_dir_file;
    private $target_dir_file_cnil;

    public function __construct(CRASecurityService $securityService,
                                $target_dir_file,
                                $target_dir_file_cnil)
    {
        $this->securityService = $securityService;
        $this->target_dir_file = $target_dir_file;
        $this->target_dir_file_cnil = $target_dir_file_cnil;
    }

    /**
     * Get data directory
     */
    public function getTargetDirFile() {
        return $this->target_dir_file;
    }

    /**
     * Upload file and / or md
     */
    public function uploadData(DataModel $data)
    {
        // Generate random Id for the new file & md
        $randomId = $this->securityService->generateRandomId();
        // Check the type of upload
        switch ($data->getUpdateType()) {
            case 'md' :
                $this->encryptAndWriteMd($data, $randomId);
                break;
            case 'doc' :
                $this->encryptAndWriteFile($data, $randomId);
                break;
            default :
                $this->encryptAndWriteMd($data, $randomId);
                $this->encryptAndWriteFile($data, $randomId);
                break;
        }
    }

    /**
     * Encrypt and write raw metadata
     */
    public function encryptAndWriteMd(DataModel $data, string $randomId)
    {
        // Set the md filename
        $data->setMdName("md_".$randomId);
        // Metadata are sent in base64 encoding, decoding back reduces the size of the data
        $rawMd = base64_decode($data->getMdRawData());
        // Only encrypt if the file isn't a CNIL file
        if (!$data->getCnil()) {
            $rawMd = $this->securityService->symmetricEncryptBIN($rawMd, $data->getNonceBIN(), $data->getClient()->getUser()->getFileUserKeyBIN());
        }
        // TODO: To be removed later
        set_time_limit(5000000);
        ini_set("memory_limit","3G");
        // Write md in storage
        $this->writeMd($data, $rawMd);
    }

    /**
     * Encrypt and write raw file
     */
    public function encryptAndWriteFile(DataModel $data, string $randomId)
    {
        // Set the filename
        $data->setFileName("doc_".$randomId);
        // Files are sent in base64 encoding, decoding back reduces the size of the data
        $rawFile = base64_decode($data->getFileRawData());
        // Only encrypt if the file isn't a CNIL file
        if (!$data->getCnil()) {
            $rawFile = $this->securityService->symmetricEncryptBIN($rawFile, $data->getNonceBIN(), $data->getClient()->getUser()->getFileUserKeyBIN());
        }
        // TODO: To be removed later
        set_time_limit(5000000);
        ini_set("memory_limit","3G");
        // Write File in storage
        $this->writeFile($data, $rawFile);
    }

    /**
     * Download file and / or metadata
     */
    public function downloadData(DataModel $data)
    {
        // Check the type of download
        switch ($data->getDownloadType()) {
            case 'md':
                $this->readAndDecryptMd64($data);
                break;
            case 'doc':
                $this->readAndDecryptFile64($data);
                break;
            default:
                $this->readAndDecryptFile64($data);
                $this->readAndDecryptMd64($data);
                break;
        }
    }

    /**
     * Read and decrypt a metadata
     */
    public function readAndDecryptMd(DataModel $data)
    {
        // Get raw metadata
        $this->readMd($data);
        // Decrypt md if the file isn't a CNIL file
        if (!$data->getCnil()) {
            $data->setMdRawData($this->securityService->symmetricDecrypt($data->getMdRawData(), $data->getNonceBIN(), $data->getClient()->getUser()->getFileUserKeyBIN()));
            if ($data->getMdRawData() === false) {
                throw new AccessDeniedException("Md decryption error");
            }
        }
    }

    /**
     * Read and decrypt a metadata (base64 encoded result)
     */
    public function readAndDecryptMd64(DataModel $data) {
        $this->readAndDecryptMd($data);
        // Data is sent in base64 format in a JSON response, but stored decoded. Encode before continuing.
        $data->setMdRawData(base64_encode($data->getMdRawData()));
    }

    /**
     * Read and decrypt a file
     */
    public function readAndDecryptFile(DataModel $data)
    {
        // Get raw file
        $this->readFile($data);
        // Decrypt file if the file isn't a CNIL file
        if (!$data->getCnil()) {
            $data->setFileRawData($this->securityService->symmetricDecrypt($data->getFileRawData(), $data->getNonceBIN(), $data->getClient()->getUser()->getFileUserKeyBIN()));
            if ($data->getMdRawData() === false) {
                throw new AccessDeniedException("File decryption error");
            }
        }
    }

    /**
     * Read and decrypt a file (base64 encoded result)
     */
    public function readAndDecryptFile64(DataModel $data)
    {
        $this->readAndDecryptFile($data);
        // Data is sent in base64 format in a JSON response, but stored decoded. Encode before continuing.
        $data->setFileRawData(base64_encode($data->getFileRawData()));
    }

    /**
     * Duplicate and encrypt file with a new key, execute a key rotation
     */
    public function fileKeyRotation(DataModel $oldData, DataModel $newData, string $newFileUserKey)
    {
        // 1. Decrypt the file
        $this->readAndDecryptFile($oldData);

        // 2. Encrypt the file with newFileUserKey
        $newRawEncryptedFile = $this->securityService->symmetricEncrypt($oldData->getFileRawData(), $newData->getNonceBIN(), $newFileUserKey);

        // 3. Save new newRawEncryptedFile and update fullFilePath of newData
        $newData->setFileName("doc_" . $this->securityService->generateRandomId());
        $newData->setFileRawData(base64_encode($oldData->getFileRawData()));
        $this->writeFile($newData, $newRawEncryptedFile);
    }

    /**
     * Duplicate and encrypt md with a new key, execute a key rotation
     */
    public function mdKeyRotation(DataModel $oldData, DataModel $newData, string $newFileUserKey)
    {
        // 1. Decrypt md
        $this->readAndDecryptMd($oldData);

        // 2. Encrypt md with newFileUserKey
        $newRawEncryptedMd = $this->securityService->symmetricEncrypt($oldData->getMdRawData(), $newData->getNonceBIN(), $newFileUserKey);

        // 3. Save new md and update newData
        $newData->setMdName("md_" . $this->securityService->generateRandomId());
        $newData->setMdRawData(base64_encode($oldData->getMdRawData()));
        $this->writeMd($newData, $newRawEncryptedMd);
    }

    /** File system operations */

    /**
     * Write a metadata
     */
    public function writeMd(DataModel $data, string $rawMd)
    {
        try {
            // Resolve md directory depending on the cnil state of the file
            if ($data->getCnil()) {
                $mdDir = $this->target_dir_file_cnil . $data->getMdDir();
            }
            else {
                $mdDir = $this->target_dir_file . $data->getMdDir();
            }
            // Check if folder exists, if not create it (recursive)
            if (!is_dir($mdDir)) {
                mkdir($mdDir, 0755, true);
            }
            
            // Write metadata
            file_put_contents($mdDir . $data->getMdName(), $rawMd);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Write a file
     */
    public function writeFile(DataModel $data, string $rawFile)
    {
        try {
            // Resolve file directory depending on the cnil state of the file
            if ($data->getCnil()) {
                $fileDir = $this->target_dir_file_cnil . $data->getFileDir();
            } else {
                $fileDir = $this->target_dir_file . $data->getFileDir();
            }
            // Check if folder exists, if not create it (recursive)
            if (!is_dir($fileDir)) {
                mkdir($fileDir, 0755, true);
            }
            
            // Write file
            file_put_contents($fileDir . $data->getFileName(), $rawFile);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Read a metadata
     */
    public function readMd(DataModel $data)
    {
        try {
            // Resolve md path depending on the cnil state of the file
            if ($data->getCnil()) {
                $mdPath = $this->target_dir_file_cnil . $data->getFullMdPath();
            }
            else {
                $mdPath = $this->target_dir_file . $data->getFullMdPath();
            }
            
            // read metadata
            $data->setMdRawData(file_get_contents($mdPath));

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Read a file
     */
    public function readFile(DataModel $data)
    {
        try {
            // Resolve file path depending on the cnil state of the file
            if ($data->getCnil()) {
                $filePath = $this->target_dir_file_cnil . $data->getFullFilePath();
            }
            else {
                $filePath = $this->target_dir_file . $data->getFullFilePath();
            }
            
            // read file
            $data->setFileRawData(file_get_contents($filePath));

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Delete the directory of a file (deletes all versions of file and md)
     */
    public function deleteDirectory(DataModel $data)
    {
        $fs = new Filesystem();
        $prefix = substr($data->getClient()->getClientId(), 0, CRASecurityService::RANDOM_ID_LENGTH);

        // Check if the file is a CNIL file
        if ($data->getCnil()) {
            $fileDir = $this->target_dir_file_cnil . '/' . $prefix . '/' . $data->getFileId();
        } else {
            $fileDir = $this->target_dir_file . '/' . $prefix . '/' . $data->getFileId();
        }

        // Delete the folder
        try {
            $fs->remove($fileDir);
        } catch (Exception $e) {
            return new Exception($e->getMessage());
        }
    }

    /**
     * Delete the directory of the user (deletes all versions of all files and mds)
     */
    public function deleteUser(Client $client)
    {
        $fs = new Filesystem();
        $userPath = $this->target_dir_file . substr($client->getClientId(), 0, CRASecurityService::RANDOM_ID_LENGTH);
        $userCnilPath = $this->target_dir_file_cnil . substr($client->getClientId(), 0, CRASecurityService::RANDOM_ID_LENGTH);

        // Delete the folder
        try {
            if (is_dir($userPath)) {
                $fs->remove($userPath);
            }
            if (is_dir($userCnilPath)) {
                $fs->remove($userCnilPath);
            }
        } catch (Exception $e) {
            return new Exception($e->getMessage());
        }
    }

    /**
     *   Delete file and md for a specific version
     */
    public function deleteVersion(DataModel $data)
    {
        $this->deleteDocVersion($data);
        $this->deleteMdVersion($data);
    }

    /**
     *   Delete file for a specific version
     */
    public function deleteDocVersion(DataModel $data)
    {
        // Check if the file is a CNIL file
        if ($data->getCnil()) {
            $filePath = $this->target_dir_file_cnil . $data->getFullFilePath();
        } else {
            $filePath = $this->target_dir_file . $data->getFullFilePath();
        }

        // Delete the file
        try {
            unlink($filePath);
        } catch (Exception $e) {
            return new Exception($e->getMessage());
        }
    }

    /**
     *   Delete md for a specific version
     */
    public function deleteMdVersion(DataModel $data)
    {
        // Check if the file is a CNIL file
        if ($data->getCnil()) {
            $mdPath = $this->target_dir_file_cnil . $data->getFullMdPath();
        } else {
            $mdPath = $this->target_dir_file . $data->getFullMdPath();
        }

        // Delete the file
        try {
            unlink($mdPath);
        } catch (Exception $e) {
            return new Exception($e->getMessage());
        }
    }
}
