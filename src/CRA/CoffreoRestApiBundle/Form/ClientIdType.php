<?php
// CRA\CoffreoRestApiBundle\Form\ClientIdType.php
namespace CRA\CoffreoRestApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ClientIdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('client_id', TextType::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults(array(
			'data_class' => 'CRA\OAuthServerBundle\Entity\Client'
		));
    }
}
