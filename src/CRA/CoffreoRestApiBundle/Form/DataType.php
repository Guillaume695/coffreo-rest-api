<?php
// CRA\CoffreoRestApiBundle\Form\DataType.php
namespace CRA\CoffreoRestApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class DataType extends AbstractType
{
	private $securityService;
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$this->securityService = $options['security_service'];
		$builder
			->add('file_id', TextType::class, ['required' => false, 'empty_data' => $this->securityService->generateRandomId()])
			->add('file_raw_data', TextType::class, ['required' => false, 'empty_data' => ""])
			->add('file_ext', TextType::class, ['required' => false, 'empty_data' => ""])
			->add('md_raw_data', TextType::class, ['required' => false, 'empty_data' => ""])
			->add('md_ext', TextType::class, ['required' => false, 'empty_data' => ""])
			->add('version', IntegerType::class, ['required' => false, 'empty_data' => 0])
			->add('cnil', IntegerType::class, ['required' => false, 'empty_data' => 0]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults(array(
			'data_class' => 'CRA\CoffreoRestApiBundle\Model\DataModel'
		));
		$resolver->setRequired('security_service');
    }
}
