<?php
// CRA\CoffreoRestApiBundle\Form\UpdateType.php
namespace CRA\CoffreoRestApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class UpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file_id', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid file_id input.',
                'constraints' => new Length([
                    'min' => 2 * CRASecurityService::RANDOM_ID_LENGTH,
                    'max' => 2 * CRASecurityService::RANDOM_ID_LENGTH,
                    'minMessage' => 'invalid file_id size.',
                    'maxMessage' => 'invalid file_id size.'
                ])
            ])
            ->add('init_file_name', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid init_fil_name input.'
            ])
			->add('file_raw_data', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid md_raw_data input.'
            ])
            ->add('init_md_name', TextType::class, [
                'required' => true,
                'invalid_message' => 'md_raw_data input.'
            ])
			->add('md_raw_data', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid md_raw_data input.'
            ])
            ->add('update_type', TextType::class, [
                'required' => true,
                'empty_data' => "all",
                'invalid_message' => 'invalid update_type input.'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults(array(
			'data_class' => 'CRA\CoffreoRestApiBundle\Model\DataModel'
        ));
    }
}
