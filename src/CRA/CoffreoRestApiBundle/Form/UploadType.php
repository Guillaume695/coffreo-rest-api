<?php
// CRA\CoffreoRestApiBundle\Form\UploadType.php
namespace CRA\CoffreoRestApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
            ->add('init_file_name', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid init_fil_name input.',
                'constraints' => new NotBlank(['message' => 'init_file_name should not be empty.'])
            ])
			->add('file_raw_data', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid md_raw_data input.',
                'constraints' => new NotBlank(['message' => 'file_raw_data should not be empty.'])
            ])
            ->add('init_md_name', TextType::class, [
                'required' => true,
                'invalid_message' => 'md_raw_data input.',
                'constraints' => new NotBlank(['message' => 'init_md_name should not be empty.'])
            ])
			->add('md_raw_data', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid md_raw_data input.',
                'constraints' => new NotBlank(['message' => 'md_raw_data should not be empty.'])
            ])
            ->add('cnil', IntegerType::class, [
                'required' => true,
                'empty_data' => 0,
                'invalid_message' => 'invalid cnil input.',
                'constraints' => new Choice([
                    'choices' => [0, 1],
                    'message' => 'cnil should be 0 or 1'
                ])
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults(array(
			'data_class' => 'CRA\CoffreoRestApiBundle\Model\DataModel'
        ));
    }
}
