<?php
// CRA\CoffreoRestApiBundle\Form\ClientIdType.php
namespace CRA\CoffreoRestApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class FileIdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file_id', TextType::class, [
                'required' => true,
                'invalid_message' => 'invalid file_id input.',
                'constraints' => new Length([
                    'min' => 2 * CRASecurityService::RANDOM_ID_LENGTH,
                    'max' => 2 * CRASecurityService::RANDOM_ID_LENGTH,
                    'minMessage' => 'invalid file_id size.',
                    'maxMessage' => 'invalid file_id size.'
                ])
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults(array(
			'data_class' => 'CRA\CoffreoRestApiBundle\Model\DataModel'
        ));
    }
}
