<?php
// src/CRA/CoffreoRestApiBundle/EventListener/CRAEventListener.php

namespace CRA\CoffreoRestApiBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class CRAResponse extends JsonResponse {

    public function __construct($data = null, $message = "", $status = 200, $headers = array(), $json = true)
    {
        parent::__construct('', $status, $headers);

        if (null === $data) {
            $data = new \ArrayObject();
        }

        $json ? $this->setJson($data) : $this->setData($data);

        $this->setContent(json_encode([
            'type' => "ok",
            'message' => $message,
            'content' => json_decode($data, true)
            ]));
    }
}
