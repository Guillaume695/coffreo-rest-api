<?php
// src/CRA/CoffreoRestApiBundle/Event/CRAEvent.php

namespace CRA\CoffreoRestApiBundle\Event;

final class CRAEvent {
    const onActionDone = 'CRA.actionDone';

    /*
    const createUser = "create_user";

    const createFolderStorage = "create_folder_storage";
    const createFileStorage = "create_file_storage";
    const createLog = "create_log";
    const createStorageLog = "create_storage_log";
    const createFileDatabase = "update_file_database";
    */

    const uploadFile = "upload_file";
    const uploadLog = "upload_log";
    const uploadDatabase = "upload_database";

    const updateFile = "update_file";
    const updateLog = "update_log";
    const updateDatabase = "update_database";
}
