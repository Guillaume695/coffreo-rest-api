<?php
// src/CRA/CoffreoRestApiBundle/Event/CRAActionEvent.php

namespace CRA\CoffreoRestApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CRA\CoffreoRestApiBundle\Model\DataModel;

final class CRAActionEvent extends Event {

    private $type;
    private $data;

    public function __construct(string $type, DataModel $data) {
        $this->type = $type;
        $this->data = $data;
    }

    public function getType() {
        return $this->type;
    }

    public function getData() {
        return $this->data;
    }
}
