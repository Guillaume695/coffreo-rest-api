<?php
// src/CRA/CoffreoRestApiBundle/EventListener/CRAEventListener.php

namespace CRA\CoffreoRestApiBundle\EventListener;

use Doctrine\ORM\EntityManager;
use CRA\CoffreoRestApiBundle\Event\CRAEvent;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\CoffreoRestApiBundle\Model\LastLogModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use CRA\CoffreoRestApiBundle\Event\CRAActionEvent;
use CRA\CoffreoRestApiBundle\LogService\CRALogService;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\Exception\BadRequestException;
use CRA\CoffreoRestApiBundle\Exception\FileNotFoundException;
use CRA\CoffreoRestApiBundle\Exception\ClientNotFoundException;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use CRA\CoffreoRestApiBundle\Exception\SignatureFailureException;
use CRA\CoffreoRestApiBundle\Exception\InvalidCredentialException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class CRAEventListener {

    private $stack;

    private $em;
    private $logService;
    private $fileService;
    private $securityService;

    public function __construct(EntityManager $em, CRALogService $logService, CRAFileService $fileService, CRASecurityService $securityService) {
        $this->em = $em;
        $this->logService = $logService;
        $this->fileService = $fileService;
        $this->securityService = $securityService;
        $this->stack = [];
    }

    public function getStack() {
        return $this->stack;
    }

    public function onActionDone(CRAActionEvent $event) {
        array_unshift($this->stack, $event);
    }

    public function undoAction() {

        foreach($this->stack as $action) {

            $data = $action->getData();

            switch($action->getType()) {
                case CRAEvent::uploadFile :
                    $this->fileService->deleteDirectory($data);
                    break;

                case CRAEvent::uploadLog :
                    $this->logService->deleteLogs($data);
                    break;

                case CRAEvent::uploadDatabase :
                    $this->em->getRepository("CRACoffreoRestApiBundle:File")->deleteFilesByFileId($data);
                    break;
                
                case CRAEvent::updateFile :
                    if($data->getUpdateType() == "md")
                        $this->fileService->deleteMdVersion($data);

                    elseif($data->getUpdateType() == "doc")
                        $this->fileService->deleteDocVersion($data);
                    
                    else
                        $this->fileService->deleteVersion($data);
                    break;

                case CRAEvent::updateLog :
                    if($data->getUpdateType() != 'md') {
                        $lastLog = $this->logService->readAndDecryptLastLog($data);
                        $lastStorageLog = $this->logService->readStorageLogFromData($data);
                        $storagePath = $lastLog->getCurrentStorageLog();
                        $lastLog->setCurrentStorageLog($lastStorageLog->getPreviousStorageLog());
                        $lastLog->setLastRawEntry("");
                        $lastLog->setSignedProof($this->logService->readAndDecryptStorageLogLastEntry($data)->getPrevSignedProof());
                        $this->logService->writeLastLog($data, $lastLog);
                        $this->logService->deleteStorage($storagePath);
                    } else {
                        $lastLog = $this->logService->readAndDecryptLastLog($data);
                        /** @var LastLogModel $lastLog */
                        if($lastLog->getLastRawEntry() != "") {
                            $lastLog->setLastRawEntry("");
                            $lastLog->setSignedProof($lastLog->getLastEntry()->getPrevSignedProof());
                            $this->logService->writeLastLog($data, $lastLog);
                        } else {
                            $lastStorageLog = $this->logService->readStorageLogFromData($data);
                            $lastRawEntry = end($lastStorageLog->getRawEntries());
                            $lastLog->setSignedProof($lastRawEntry->getPrevSignedProof());
                            $lastStorageLog->deleteLastRawEntry();
                            $this->logService->writeLastLog($data, $lastLog);
                            $this->logService->writeStorageLog($data, $lastStorageLog);
                        }
                    }

                    break;

                case CRAEvent::updateDatabase :
                    $oldData = DataModel::duplicateDataModel($data);
                    $oldData->setVersion($data->getVersion() - 1);
                    $oldFile = $this->em->getRepository("CRACoffreoRestApiBundle:File")->findFileByVersion($oldData);
                    $newFile = $this->em->getRepository("CRACoffreoRestApiBundle:File")->findFileByLastVersion($data);
                    $oldFile->setFullLastLogPath($newFile->getFullLastLogPath());
                    $this->em->remove($newFile);
                    $this->em->flush();
                    break;

                default:
                    break;
            }
        }
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $this->undoAction();

        switch(true) {
            case $exception instanceof FileNotFoundException : 
                $response = [
                    'type' => 'file_not_found',
                    'message' => "The file doesn't exist.",
                    'content' => $exception->getMessage()
                ];
                break;

            case $exception instanceof ClientNotFoundException : 
                $response = [
                    'type' => 'client_not_found',
                    'message' => "The client doesn't exist.",
                    'content' => $exception->getMessage()
                ];
                break;

            case $exception instanceof BadRequestException : 
                $response = [
                    'type' => 'bad_request',
                    'message' => "Form is invalid.",
                    'content' => $exception->getMessage()
                ];
                break;

            case $exception instanceof SignatureFailureException : 
                $response = [
                    'type' => 'signature_failure',
                    'message' => "Signature failure.",
                    'content' => $exception->getMessage()
                ];
                break;

            case $exception instanceof InvalidCredentialException : 
                $response = [
                    'type' => 'invalid_credential',
                    'message' => "Privilège invalide.",
                    'content' => $exception->getMessage()
                ];
                break;

            default: //return;
                $response = [
                    'type' => 'exception',
                    'message' => "An error occured.",
                    'content' => $exception->getMessage()
                ];
                break;
        }

        $event->setResponse(new JsonResponse($response));
        return;
    }
}
