<?php

namespace CRA\CoffreoRestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="CRA\CoffreoRestApiBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="file_user_key", type="string", length=255, nullable=true)
     */
    private $fileUserKey;

    /**
     * @var string
     *
     * @ORM\Column(name="log_user_key", type="string", length=255, nullable=true)
     */
    private $logUserKey;

    /**
     * @var string
     * @Groups({"sysadmin", "funcAdmin"})
     * @ORM\Column(name="created_by", type="string", length=255)
     */
    private $createdBy;

    /**
     * @var \DateTime
     * @Groups({"sysadmin", "funcAdmin"})
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var boolean
     * @Groups({"funcAdmin"})
     * @ORM\Column(name="available", type="boolean")
     */
    private $available;

    /**
     * @var \DateTime
     * @Groups({"funcAdmin"})
     * @ORM\Column(name="last_key_rotation", type="datetime")
     */
    private $lastKeyRotation;

    /**
     * @ORM\OneToMany(targetEntity="CRA\CoffreoRestApiBundle\Entity\File", mappedBy="user", cascade={"persist", "remove"})
     * @Groups({"funcAdmin"})
     */
    private $files;

    public function __construct(string $createdBy) {
        $this->createdAt = new \DateTime();
        $this->createdBy = $createdBy;
        $this->available = true;
        $this->lastKeyRotation = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileUserKey
     *
     * @param string $fileUserKey
     *
     * @return User
     */
    public function setFileUserKey($fileUserKey)
    {
        $this->fileUserKey = $fileUserKey;

        return $this;
    }

    /**
     * Get fileUserKey (HEX)
     *
     * @return string
     */
    public function getFileUserKey()
    {
        return $this->fileUserKey;
    }

    /**
     * Get fileUserKey (BIN)
     *
     * @return string
     */
    public function getFileUserKeyBIN()
    {
        return sodium_hex2bin($this->fileUserKey);
    }

    /**
     * Set logUserKey
     *
     * @param string $logUserKey
     *
     * @return User
     */
    public function setLogUserKey($logUserKey)
    {
        $this->logUserKey = $logUserKey;

        return $this;
    }

    /**
     * Get logUserKey (HEX)
     *
     * @return string
     */
    public function getLogUserKey()
    {
        return $this->logUserKey;
    }

    /**
     * Get logUserKey (BIN)
     *
     * @return string
     */
    public function getLogUserKeyBIN()
    {
        return sodium_hex2bin($this->logUserKey);
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add file
     *
     * @param \CRA\CoffreoRestApiBundle\Entity\File $file
     *
     * @return User
     */
    public function addFile(\CRA\CoffreoRestApiBundle\Entity\File $file)
    {
        $this->files[] = $file;
        $file->setUser($this);
        return $this;
    }

    /**
     * Remove file
     *
     * @param \CRA\CoffreoRestApiBundle\Entity\File $file
     */
    public function removeFile(\CRA\CoffreoRestApiBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection $file
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set available
     * @return User
     */
    public function setAvailable(bool $available)
    {
        $this->available = $available;
        return $this;
    }

    /**
     * Get available
     *
     * @return boolean
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Set lastKeyRotation
     * @return User
     */
    public function setLastKeyRotation(\DateTime $lastKeyRotation)
    {
        $this->lastKeyRotation = $lastKeyRotation;

        return $this;
    }

    /**
     * Get lastKeyRotation
     *
     * @return \DateTime
     */
    public function getLastKeyRotation()
    {
        return $this->lastKeyRotation;
    }
}
