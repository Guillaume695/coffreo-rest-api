<?php

namespace CRA\CoffreoRestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use CRA\CoffreoRestApiBundle\Model\DataModel;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="CRA\CoffreoRestApiBundle\Repository\FileRepository")
 */
class File
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"funcAdmin"})
     * @ORM\Column(name="file_id", type="string", length=255)
     */
    private $fileId;

    /**
     * @var int
     * @Groups({"funcAdmin"})
     * @ORM\Column(name="version", type="integer")
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="full_last_log_path", type="string", length=255)
     */
    private $fullLastLogPath;

    /**
     * @var string
     *
     * @ORM\Column(name="full_storage_log_path", type="string", length=255)
     */
    private $fullStorageLogPath;

    /**
     * @var string
     *
     * @ORM\Column(name="nonce", type="string", length=255)
     */
    private $nonce;

    /**
     * @ORM\ManyToOne(targetEntity="CRA\CoffreoRestApiBundle\Entity\User", inversedBy="files")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct(DataModel $data) {
        $this->fileId = $data->getFileId();
        $this->version = $data->getVersion();
        $this->fullLastLogPath = $data->getFullLastLogPath();
        $this->fullStorageLogPath = $data->getFullStorageLogPath();
        $this->nonce = $data->getNonce();
        $data->getClient()->getUser()->addFile($this);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileId
     *
     * @param string $fileId
     *
     * @return File
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get fileId
     *
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return File
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set nonce
     *
     * @param string $nonce
     *
     * @return File
     */
    public function setNonce($nonce)
    {
        $this->nonce = $nonce;

        return $this;
    }

    /**
     * Get nonce (HEX)
     *
     * @return string
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * Get nonce (BIN)
     *
     * @return string
     */
    public function getNonceBIN()
    {
        return sodium_hex2bin($this->nonce);
    }

    /**
     * Set user
     *
     * @param \CRA\CoffreoRestApiBundle\Entity\User $user
     *
     * @return File
     */
    public function setUser(\CRA\CoffreoRestApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CRA\CoffreoRestApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set fullLastLogPath
     *
     * @param string $fullLastLogPath
     *
     * @return File
     */
    public function setFullLastLogPath($fullLastLogPath)
    {
        $this->fullLastLogPath = $fullLastLogPath;

        return $this;
    }

    /**
     * Get fullLastLogPath
     *
     * @return string
     */
    public function getFullLastLogPath()
    {
        return $this->fullLastLogPath;
    }

    /**
     * Set fullStorageLogPath
     *
     * @param string $fullStorageLogPath
     *
     * @return File
     */
    public function setFullStorageLogPath($fullStorageLogPath)
    {
        $this->fullStorageLogPath = $fullStorageLogPath;

        return $this;
    }

    /**
     * Get fullStorageLogPath
     *
     * @return string
     */
    public function getFullStorageLogPath()
    {
        return $this->fullStorageLogPath;
    }
}
