<?php

namespace CRA\CoffreoRestApiBundle\Controller;

use JMS\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use JMS\Serializer\SerializationContext;
use CRA\CoffreoRestApiBundle\Entity\File;
use CRA\CoffreoRestApiBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use CRA\OAuthServerBundle\Entity\ClientManager;
use CRA\CoffreoRestApiBundle\Form\ClientIdType;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\HttpFoundation\JsonResponse;
use CRA\CoffreoRestApiBundle\Response\CRAResponse;
use CRA\CoffreoRestApiBundle\LogService\CRALogService;
use CRA\OAuthServerBundle\Repository\ClientRepository;
use CRA\CoffreoRestApiBundle\Repository\FileRepository;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\Exception\BadRequestException;
use CRA\CoffreoRestApiBundle\Exception\ClientNotFoundException;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use CRA\CoffreoRestApiBundle\Exception\InvalidCredentialException;
use CRA\CoffreoRestApiBundle\FuncAdminService\CRAFuncAdminService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class FuncAdminController extends FOSRestController
{
    /**
     * OAuth test for a functional admin
     *
     * @Get("/funcadmin/oauthtest", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Functional Admin",
     *  description="OAuth test for a functional admin",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getFuncOAuthTestAction()
    {
        // Service
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            return new CRAResponse("", "OAuth Test successful");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }

    /**
     * Returns infos about all users
     *
     * @Get("/funcadmin/users", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Functional Admin",
     *  description="Returns infos about all users",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "collection"=true, "groups"="funcadmin"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getFuncUsersAction()
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // Serializer
            $context = SerializationContext::create()->setGroups(array('funcAdmin'));
            // Select all users
            $users = $clientRepository->findBy(array("privilege" => "user"));
            return new CRAResponse($serializer->serialize($users, 'json', $context), "Users listed");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }

    /**
     * Returns infos about a user
     *
     * @Get("/funcadmin/user", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Functional Admin",
     *  description="Returns infos about a user",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "collection"=true, "groups"="funcadmin"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getFuncUserAction(Request $request)
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // 2. Fetch client in database
            $client = $clientRepository->findClientByClientId($client->getClientId());
            // Serializer
            $context = SerializationContext::create()->setGroups(array('funcAdmin'));
            // Select all users
            return new CRAResponse($serializer->serialize($client, 'json', $context), "User info fetched");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }

    /**
     * Generate a new secret for a user
     *
     * @Post("/funcadmin/update/secret")
     * @ApiDoc(
     *  section = "Functional Admin",
     *  description = "Generate a new secret for a user",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the user for which you want to generate a new secret."
     *      }
     *  },
     *  statusCodes = {
     *         200 = "Success",
     *         400 = "Invalid client_id received",
     *         403 = "Access denied"
     *     },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "groups"="funcadmin"},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postFuncUpdateUserSecretAction(Request $request)
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // 2. Fetch client in database
            $client = $clientRepository->findClientByClientId($client->getClientId());
            // 3. Generate a new secret for the user
            if ($client->isSimpleUser()) {
                $client->setSecret(Random::generateToken());
                $em->flush();
                // Serializer
                $context = SerializationContext::create()->setGroups(array('funcAdmin'));
                return new CRAResponse($serializer->serialize($client, 'json', $context), "Secret successfully updated");
            }
            throw new BadRequestException("Invalid client_id received.");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }

    /**
     * Create a new user
     *
     * @Post("/funcadmin/create/user")
     * @ApiDoc(
     *  section = "Functional Admin",
     *  description = "Create a new user",
     *  authentication = true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "groups"="funcadmin"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postFuncCreateUserAction()
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientManager = $this->get('cra_coffreo.ClientManager');
        /** @var ClientManager $clientManager */

        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // Create a new Oauth client
            $newClient = $clientManager->createClient("user", $admin);
            $clientManager->updateClient($newClient);
            // Serializer
            $context = SerializationContext::create()->setGroups(array('funcAdmin'));
            return new CRAResponse($serializer->serialize($newClient, 'json', $context), "New user created");
        }
        throw new InvalidCredentialException("You must be Functional Admin");
    }

    /**
     * Delete a user
     *
     * @return message, 0 if success, 1 if error
     * @Delete("/funcadmin/delete/user")
     * @ApiDoc(
     *  section = "Functional Admin",
     *  description = "Delete a user",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the user to delete."
     *      }
     *  },
     *  statusCodes = {
     *        200 = "Success",
     *        400 = "Invalid client_id received",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function deleteFuncUserAction(Request $request)
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */
        $logService = $this->get('cra_coffreo.LogService');
        /** @var CRALogService $logService */
        $fileService = $this->get('cra_coffreo.FileService');
        /** @var CRAFileService $fileService */
        $funcAdminService = $this->get('cra_coffreo.FuncAdminService');
        /** @var CRAFuncAdminService $funcAdminService */

        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // 2. Fetch client in database
            $client = $clientRepository->findClientByClientId($client->getClientId());
            if ($client->isSimpleUser()) {
                // 3. Delete files, md and logs
                $funcAdminService->deleteUser($client);
                // 5. Delete user from database
                $em->remove($client);
                $em->flush();
                return new CRAResponse("", "User deleted");
            }
            throw new BadRequestException("Invalid client_id received.");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }


    /**
     * Encryption key rotation for one user
     *
     * @return message, 0 if success, 1 if error
     * @Post("/funcadmin/rotate/userkey")
     * @ApiDoc(
     *  section = "Functional Admin",
     *  description = "Encryption key rotation for one user",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the user of which you want to rotate the encryption keys"
     *      }
     *  },
     *  statusCodes = {
     *        200 = "Success",
     *        400 = "Invalid client_id received",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postFuncUserKeyRotationAction(Request $request)
    {
        // Services
        $funcAdminService = $this->get('cra_coffreo.FuncAdminService');
        /** @var CRAFuncAdminService $funcAdminService */
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRAFileService $fileService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */
        
        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                return new BadRequestEception($form->getErrors(true));
            }
            // 2. Fetch client in database
            $client = $clientRepository->findClientByClientId($client->getClientId());
            // 3. Execute the key rotation
            $funcAdminService->keyRotation($client);
            return new CRAResponse("", "Encryption key rotation successful");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }

    /**
     * Encryption key rotation for all users
     *
     * @return message, 0 if success, 1 if error
     * @Post("/funcadmin/rotate/allkeys")
     * @ApiDoc(
     *  section = "Functional Admin",
     *  description = "Encryption key rotation for all users",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the user of which you want to rotate the encryption keys"
     *      }
     *  },
     *  statusCodes = {
     *        200 = "Success",
     *        400 = "Invalid client_id received",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postFuncAllUserKeyRotationAction(Request $request)
    {
        // Services
        $funcAdminService = $this->get('cra_coffreo.FuncAdminService');
        /** @var CRAFuncAdminService $funcAdminService */
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRAFileService $fileService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */
        
        // Get client
        $admin = $securityService->getClient();
        if ($admin->isFunctionnalAdmin()) {
            // 1. Fetch all users in database
            $clients = $clientRepository->findBy(array("privilege" => "user"));
            // 2. Execute the key rotation on each client
            foreach($clients as $client) {
                $funcAdminService->keyRotation($client);
            }
            return new CRAResponse("", "Encryption key rotation for all users successful");
        }
        throw new InvalidCredentialException("You must be Functionnal Admin");
    }
}
