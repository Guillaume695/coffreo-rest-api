<?php

namespace CRA\CoffreoRestApiBundle\Controller;

use JMS\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use CRA\OAuthServerBundle\Model\User;
use FOS\OAuthServerBundle\Util\Random;
use JMS\Serializer\SerializationContext;
use CRA\OAuthServerBundle\Entity\Client;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use CRA\OAuthServerBundle\Entity\ClientManager;
use CRA\CoffreoRestApiBundle\Form\ClientIdType;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Delete;
use CRA\CoffreoRestApiBundle\Response\CRAResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use CRA\CoffreoRestApiBundle\LogService\CRALogService;
use CRA\OAuthServerBundle\Repository\ClientRepository;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\Exception\BadRequestException;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use CRA\CoffreoRestApiBundle\Exception\InvalidCredentialException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SysAdminController extends FOSRestController
{
    /**
     * OAuth test for a system admin
     * 
     * @Get("/sysadmin/oauthtest", requirements={}, defaults={})
     * @ApiDoc(
     *  section="System Admin",
     *  description="OAuth test for a system admin",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getSysOAuthTestAction()
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $admin = $securityService->getClient();
        if ($admin->isSystemAdmin()) {
            return new CRAResponse("", "OAuth Test successfull");
        }
        throw new InvalidCredentialException("You must be System Admin");
    }

    /**
     * Returns infos about of all system admins
     *
     * @Get("/sysadmin/admins", requirements={}, defaults={})
     * @ApiDoc(
     *  section="System Admin",
     *  description="Returns the client_id of all functional admins",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "collection"=true, "groups"="sysadmin"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getSysAdminsAction()
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Check privileges
        $admin = $securityService->getClient();
        if ($admin->isSystemAdmin()) {
            // Serializer
            $context = SerializationContext::create()->setGroups(array('sysadmin'));
            // Select all functional admins
            $funcadmins = $clientRepository->findBy(array("privilege" => "funcadmin"));
            return new CRAResponse($serializer->serialize($funcadmins, 'json', $context), "Functional admins listed");
        }
        throw new InvalidCredentialException("You must be System Admin");
    }

    /**
     * Generate a new secret for a functional admin
     *
     * @Post("/sysadmin/update/secret")
     * @ApiDoc(
     *  section = "System Admin",
     *  description = "Generate a new secret for a functional admin",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the functional admin for which you want to generate a new secret."
     *      }
     *  },
     *  statusCodes = {
     *         200 = "Success",
     *         400 = "Invalid client_id received",
     *         403 = "Access denied"
     *     },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "groups"="sysadmin"},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postSysUpdateAdminSecretAction(Request $request)
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Check privileges
        $admin = $securityService->getClient();
        if ($admin->isSystemAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // 2. Fetch client in database
            $funcAdmin = $clientRepository->findClientByClientId($client->getClientId());
            // 3. Generate a new secret for the functional admin if found
            if ($funcAdmin->isFunctionnalAdmin()) {
                $funcAdmin->setSecret(Random::generateToken());
                $em->flush();
                // Serializer
                $context = SerializationContext::create()->setGroups(array('sysadmin'));
                return new CRAResponse($serializer->serialize($funcAdmin, 'json', $context), "Secret successfully updated");
            }
            throw new BadRequestException("Invalid client_id received.");
        }

        throw new InvalidCredentialException("You must be System Admin");
    }

    /**
     * Create a new functional admin
     *
     * @Post("/sysadmin/create/admin")
     * @ApiDoc(
     *  section = "System Admin",
     *  description = "Create a new functional admin",
     *  authentication = true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\OAuthServerBundle\Entity\Client", "groups"="sysadmin"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postSysCreateAdminAction()
    {
        // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $serializer = $this->get('jms_serializer');
        /** @var Serializer $serializer */
        $clientManager = $this->get('cra_coffreo.ClientManager');
        /** @var ClientManager $clientManager */

        // Check privileges
        $admin = $securityService->getClient();
        if ($admin->isSystemAdmin()) {
            // Create a new Oauth client
            $newClient = $clientManager->createClient("funcadmin", $admin);
            $clientManager->updateClient($newClient);
            // Serializer
            $context = SerializationContext::create()->setGroups(array('sysadmin'));
            return new CRAResponse($serializer->serialize($newClient, 'json', $context), "New functionnal admin created");
        }
        throw new InvalidCredentialException("You must be System Admin");
    }

    /**
     * Delete a functional admin
     * 
     * @Delete("/sysadmin/delete/admin")
     * @ApiDoc(
     *  section = "System Admin",
     *  description = "Delete a functional admin",
     *  authentication = true,
     *  requirements={
     *      {
     *          "name"="client_id",
     *          "dataType"="string",
     *          "requirement"="true",
     *          "description"="ID of the functional admin to delete."
     *      }
     *  },
     *  statusCodes = {
     *        200 = "Success",
     *        400 = "Invalid client_id received",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        400 = {"class"="Symfony\Component\HttpKernel\Exception\BadRequestHttpException"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
     public function deleteSysAdminAction(Request $request)
     {
         // Services
        $securityService = $this->get('cra_coffreo.SecurityService');
        /** @var CRASecurityService $securityService */
        $em = $this->getDoctrine()->getManager();
        /** @var EntityManager $em */
        $clientRepository = $em->getRepository('CRAOAuthServerBundle:Client');
        /** @var ClientRepository $clientRepository */

        // Check privileges
        $admin = $securityService->getClient();
        if ($admin->isSystemAdmin()) {
            // 1. Parse request
            $client = new Client();
            $form = $this->createForm(ClientIdType::class, $client);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // 2. Fetch client in database
            $funcAdmin = $clientRepository->findClientByClientId($client->getClientId());
            // Delete admin
            if ($funcAdmin->isFunctionnalAdmin()) {
                $em->remove($funcAdmin);
                $em->flush();
                return new CRAResponse("", "Functional Admin deleted");
            }
            throw new BadRequestException("Invalid client_id received.");
        }
        throw new InvalidCredentialException("You must be System Admin");
    }
}
