<?php

namespace CRA\CoffreoRestApiBundle\Controller;

use JMS\Serializer\Serializer;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\File;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CRA\CoffreoRestApiBundle\Form\DataType;
use CRA\CoffreoRestApiBundle\Form\CnilType;
use CRA\CoffreoRestApiBundle\Model\LogModel;
use CRA\CoffreoRestApiBundle\Event\CRAEvent;
use CRA\CoffreoRestApiBundle\Form\UploadType;
use CRA\CoffreoRestApiBundle\Form\UpdateType;
use CRA\CoffreoRestApiBundle\Form\FileIdType;
use Symfony\Component\HttpFoundation\Request;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use CRA\CoffreoRestApiBundle\Model\ProofModel;
use CRA\CoffreoRestApiBundle\Form\DownloadType;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Delete;
use CRA\CoffreoRestApiBundle\Event\CRAActionEvent;
use CRA\CoffreoRestApiBundle\Response\CRAResponse;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use CRA\CoffreoRestApiBundle\LogService\CRALogService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use CRA\CoffreoRestApiBundle\Repository\FileRepository;
use CRA\CoffreoRestApiBundle\FileService\CRAFileService;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use CRA\CoffreoRestApiBundle\Exception\FileNotFoundException;
use CRA\CoffreoRestApiBundle\Exception\BadRequestException;
use CRA\CoffreoRestApiBundle\EventListener\CRAEventListener;
use CRA\CoffreoRestApiBundle\Exception\SignatureFailureException;
use CRA\CoffreoRestApiBundle\Exception\InvalidCredentialException;

class UserController extends FOSRestController
{
    private $securityService;
    private $fileService;
    private $logService;
    private $em;
    private $serializer;
    private $fileRepository;
    private $eventListener;

    public function __construct(CRASecurityService $securityService,
                                CRAFileService $fileService,
                                CRALogService $logService,
                                EntityManager $em,
                                Serializer $serializer,
                                FileRepository $fileRepository,
                                CRAEventListener $eventListener) {
        
        $this->securityService = $securityService;
        /** @var CRASecurityService $securityService */
        $this->fileService = $fileService;
        /** @var CRAFileService $fileService */
        $this->logService = $logService;
        /** @var CRALogService $logService */
        $this->em = $em;
        /** @var EntityManager $em */
        $this->serializer = $serializer;
        /** @var Serializer $serializer */
        $this->fileRepository = $fileRepository;
        /** @var FileRepository $fileRepository */
        $this->eventListener = $eventListener;
        /** @var CRAEventListener $eventListener */
    }

    /**
     * OAuth test for a simple user
     *
     * @Get("/user/oauthtest", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="OAuth test for a simple user",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"=""},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function getUserOAuthTestAction()
    {
        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            return new CRAResponse("", "OAuth Test successful");
        }
        throw new InvalidCredentialException("You must be User");
    }

    /**
     * Upload a file & its metadata
     *
     * @Post("/user/upload", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Upload a file & its metadata",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="user"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postUserUploadAction(Request $request)
    {
        $dispatcher = $this->get('event_dispatcher');
        /** @var EventDispatcher $dispatcher */

        // Check privileges
        $client = $this->securityService->getClient();

        if ($client->isSimpleUser()) {
            // Create a new dataModel
            $data = new DataModel($client);
            $form = $this->createForm(UploadType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // Init dataModel for a new file
            $data->setVersion(1);
            $data->setFileId($this->securityService->generateRandomId());
            $data->setNonce($this->securityService->generateNonce());
            // Write data to storage
            $this->fileService->uploadData($data);
            $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::uploadFile, $data));
            // Create a new log
            $this->logService->appendLog($data, "Upload");
            $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::uploadLog, $data));
            // Update Database
            $file = new File($data);
            $this->em->persist($file);
            $this->em->flush();
            $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::uploadDatabase, $data));
            // Serializer
            $context = SerializationContext::create()->setGroups(array('user'));
            return new CRAResponse($this->serializer->serialize($data, 'json', $context), "Upload successful");
        }

        throw new InvalidCredentialException("You must be User");
    }

    /**
     * Update a file and / or its metadata
     *
     * @Post("/user/update", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Update a file and / or its metadata",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="user"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postUserUpdateAction(Request $request)
    {
        // Services
        $dispatcher = $this->get('event_dispatcher');
        /** @var EventDispatcher $dispatcher */
        
        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            // Init a dataModel with user input
            $data = new DataModel($client);
            $form = $this->createForm(UpdateType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // Get last version of file from database
            $lastVersionFile = $this->fileRepository->findFileByLastVersion($data);
            // Update datamodel
            $data->updateDataWithFile($lastVersionFile);
            $this->logService->updateDataWithLastLog($data);
            // Write updated data to storage
            $this->fileService->uploadData($data);
            $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::updateFile, $data));
            // Create a new update log
            $this->logService->appendUpdateLog($data);
            $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::updateLog, $data));
            // Update Database
            if ($data->getUpdateType() != "md") {
                // The last log file of the previous file is updated to an empty value, indicating we should look into storage log to find the URI of the file.
                $lastVersionFile->setFullLastLogPath("");
                // Create a new File in database
                $file = new File($data);
                $this->em->persist($file);
                $this->em->flush();
            
                $dispatcher->dispatch(CRAEvent::onActionDone, new CRAActionEvent(CRAEvent::updateDatabase, $data));
            }

            // Serializer
            $context = SerializationContext::create()->setGroups(array('user'));
            return new CRAResponse($this->serializer->serialize($data, 'json', $context), "Update successful");
        }

        throw new InvalidCredentialException("You must be User");
    }

    /**
     * Download a file and / or its metadata
     *
     * @Get("/user/download", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Download a file and / or its metadata",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="download"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postUserDownloadAction(Request $request)
    {
        // Services
        $dispatcher = $this->get('event_dispatcher');
        /** @var EventDispatcher $dispatcher */

        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            // Init a dataModel with user input
            $data = new DataModel($client);
            $form = $this->createForm(DownloadType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // Select last version of file in database
            $lastVersionFile = $this->fileRepository->findFileByLastVersion($data);
            // Check what version is asked by the user
            if ($data->getVersion() > 0 && ($data->getVersion() != $lastVersionFile->getVersion())) {
                $file = $this->fileRepository->findFileByVersion($data);
                if (!$file) {
                    throw new FileNotFoundException();
                }
            } else {
                $file = $lastVersionFile;
            }
            // Update datamodel
            $data->updateDataWithFile($file);
            // Update $data from last log of request version
            $this->logService->updateDataWithLastLog($data);
            // Read file and / or md
            $this->fileService->downloadData($data);
            // Create a new download log in the lastVersionFile linked log list
            // (Because this log is appended to the list, we need to update the log paths to the latest values)
            $data->updateDataWithFile($lastVersionFile);
            $this->logService->appendDownloadLog($data, $file->getVersion());
            // Serializer (respond the correct version)
            $data->setVersion($file->getVersion());
            $context = SerializationContext::create()->setGroups(array('download'));
            return new CRAResponse($this->serializer->serialize($data, 'json', $context), "Download successful");
        }
        
        throw new InvalidCredentialException("You must be User");
    }

    /**
     * Delete a file & its metadata
     *
     * @Delete("/user/delete", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Delete a file & its metadata",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="user"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function deleteUserDeleteAction(Request $request)
    {
         // Services
         $dispatcher = $this->get('event_dispatcher');
         /** @var EventDispatcher $dispatcher */
        
        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            // Create DataModel From Last Log
            $data = new DataModel($client);
            $form = $this->createForm(FileIdType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            if (!$form->isValid()) {
                throw new BadRequestException($form->getErrors(true));
            }
            // Select last version of file
            $lastVersionFile = $this->fileRepository->findFileByLastVersion($data);
            $data->updateDataWithFile($lastVersionFile);
            // Delete File Folder
            $this->fileService->deleteDirectory($data);
            // Delete logs
            $this->logService->deleteLogs($data);
            // Delete all entries of file in database
            $this->fileRepository->deleteFilesByFileId($data);
            // Serializer
            $context = SerializationContext::create()->setGroups(array('user'));
            return new CRAResponse($this->serializer->serialize($data, 'json', $context), "Delete successful");
        }

        throw new InvalidCredentialException("You must be User");
    }

    /**
     * Activate or Deactivate Cnil mode for a file: upload the new version, the old ones will be deleted
     *
     * @Post("/user/cnil", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Activate or Deactivate Cnil mode for a file: upload the new version, the old ones will be deleted",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="user"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postUserCnilOnOffAction(Request $request)
    {
        // Services
        $dispatcher = $this->get('event_dispatcher');
        /** @var EventDispatcher $dispatcher */
        
        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            // 1. Parse user input
            $data = new DataModel($client);
            $form = $this->createForm(CnilType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            // 2. Select last version of file in database and last log (keep cnil initial value as it will be erased)
            $cnilIniputValue = $data->getCnil();
            $lastVersionFile = $this->fileRepository->findFileByLastVersion($data);
            $data->updateDataWithFile($lastVersionFile);
            $this->logService->updateDataWithLastLog($data);
            if ($data->getCnil() == $cnilIniputValue) {
                throw new BadRequestException("This file is already in that cnil state");
            }
            $data->setCnil($cnilIniputValue);
            // 3. Delete old not cnil files & md
            $this->fileService->deleteDirectory($data);
            // 4. Upload the CNIL version of the file (already encrypted with the CNIL key)
            $this->fileService->uploadData($data);
            $this->logService->appendUpdateLog($data);
            $this->logService->updateCnilState($data);
            // 5. Log Cnil changed status
            if ($data->getCnil() == 1) {
                $this->logService->appendLog($data, "CNIL activated");
            } else {
                $this->logService->appendLog($data, "CNIL deactivated");
            }
            // 6. Update database
            $lastVersionFile->setFullLastLogPath("");
            // Create a new File in database
            $file = new File($data);
            $this->em->persist($file);
            $this->em->flush();
            // Serializer
            $context = SerializationContext::create()->setGroups(array('user'));
            return new CRAResponse($this->serializer->serialize($data, 'json', $context), "CNIL change successful");
        }

        throw new InvalidCredentialException("You must be User");
    }


    /**
     * Returns functional logs history and final proof for a file
     *
     * @Post("/user/proof", requirements={}, defaults={})
     * @ApiDoc(
     *  section="Simple User",
     *  description="Returns functional logs history and final proof for a file",
     *  authentication=true,
     *  statusCodes = {
     *        200 = "Success",
     *        403 = "Access denied"
     *  },
     *  responseMap = {
     *        200 = {"class"="CRA\CoffreoRestApiBundle\Model\DataModel", "groups"="proof"},
     *        403 = {"class"="Symfony\Component\Security\Core\Exception\AccessDeniedException"}
     *  }
     * )
     */
    public function postUserProofAction(Request $request)
    {
        // Services
        $dispatcher = $this->get('event_dispatcher');
        /** @var EventDispatcher $dispatcher */

        // Check privileges
        $client = $this->securityService->getClient();
        if ($client->isSimpleUser()) {
            // Init a dataModel with user input
            $data = new DataModel($client);
            $form = $this->createForm(FileIdType::class, $data);
            $form->submit(json_decode($request->getContent(), true));
            // Select last version of file in database
            $lastVersionFile = $this->fileRepository->findFileByLastVersion($data);
            // Update datamodel
            $data->updateDataWithFile($lastVersionFile);
            // Update $data from last log
            $this->logService->updateDataWithLastLog($data);
            // Create proof
            $proof = new ProofModel($data);
            $this->logService->getProof($data, $proof);
            // Serializer
            $context = SerializationContext::create()->setGroups(array('proof'));
            return new CRAResponse($this->serializer->serialize($proof, 'json', $context), "Proof generation successful");
        }

        throw new InvalidCredentialException("You must be User");
    }
}
