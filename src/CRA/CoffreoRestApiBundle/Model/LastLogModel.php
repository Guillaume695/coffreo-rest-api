<?php
// CRA\CoffreoRestApiBundle/Model/LastLogModel.php
namespace CRA\CoffreoRestApiBundle\Model;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Model\LogModel;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * LastLogModel
 */
class LastLogModel
{
    /**
     * Last log file header
     * @Groups({"log"})
     * @Type("array")
     * @var array
     */
    private $header;

    /**
     * Raw last log entry
     * @Groups({"log"})
     * @Type("string")
     * @var string
     */
    private $lastRawEntry;

    /**
     * Decrypted last log entry
     * @var LogModel
     */
    private $lastEntry;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->header = [
            "timestamp" => "",
            "client_id" => "",
            "file_id" => "",
            "model" => "",
            "cnil" => 0,
            "hash_algorithm" => "",
            "symmetric_algorithm" => "",
            "current_storage_log" => "",
            "signed_proof" => ""
        ];
        $this->lastRawEntry = "";
        $this->lastEntry = null;
    }

    /**
     * Create a new LastLogModel instance
     * @return LastLogModel
     */
    public static function createNewLastLog(DataModel $data) {
        $newLastLog = new LastLogModel();
        $newLastLog->setTimestamp((new \DateTime())->getTimestamp());
        $newLastLog->setClientId($data->getClient()->getClientId());
        $newLastLog->setFileId($data->getFileId());
        $newLastLog->setCurrentStorageLog($data->getFullStorageLogPath());
        if ($data->getCnil()) {
            $newLastLog->setCnil(1);
        }
        return $newLastLog;
    }

    /**
     * Get header
     * @return array
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * Set header
     * @return LastLogModel
     */
    public function setHeader(Array $header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * Get timestamp
     * @return integer
     */
    public function getTimestamp()
    {
        return $this->header['timestamp'];
    }

    /**
     * Set timestamp
     * @return LastLogModel
     */
    public function setTimestamp(int $timestamp)
    {
        $this->header['timestamp'] = $timestamp;
        return $this;
    }

    /**
     * Get client id
     * @return integer
     */
    public function getClientId()
    {
        return $this->header['client_id'];
    }

    /**
     * Set client_id
     * @return LastLogModel
     */
    public function setClientId(string $clientId)
    {
        $this->header['client_id'] = $clientId;
        return $this;
    }

    /**
     * Get file id
     * @return integer
     */
    public function getFileId()
    {
        return $this->header['file_id'];
    }

    /**
     * Set file_id
     * @return LastLogModel
     */
    public function setFileId(string $fileId)
    {
        $this->header['file_id'] = $fileId;
        return $this;
    }

    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return $this->header['model'];
    }

    /**
     * Set model
     * @return LastLogModel
     */
    public function setModel(string $model)
    {
        $this->header['model'] = $model;
        return $this;
    }

    /**
     * Get cnil
     * @return int
     */
    public function getCnil()
    {
        return $this->header['cnil'];
    }

    /**
     * Set cnil
     * @return LastLogModel
     */
    public function setCnil(int $cnil)
    {
        $this->header['cnil'] = $cnil;
        return $this;
    }

    /**
     * Get hash algorithm
     * @return string
     */
    public function getHashAlgorithm()
    {
        return $this->header['hash_algorithm'];
    }

    /**
     * Set hash algorithm
     * @return LastLogModel
     */
    public function setHashAlgorithm(string $hashAlgorithm)
    {
        $this->header['hash_algorithm'] = $hashAlgorithm;
        return $this;
    }

    /**
     * Get symmetric algorithm
     * @return string
     */
    public function getSymmetricAlgorithm()
    {
        return $this->header['symmetric_algorithm'];
    }

    /**
     * Set symmetric algorithm
     * @return LastLogModel
     */
    public function setSymmetricAlgorithm(string $symmetricAlgorithm)
    {
        $this->header['symmetric_algorithm'] = $symmetricAlgorithm;
        return $this;
    }

    /**
     * Get current storage log
     * @return string
     */
    public function getCurrentStorageLog()
    {
        return $this->header['current_storage_log'];
    }

    /**
     * Set current storage log
     * @return LastLogModel
     */
    public function setCurrentStorageLog(string $currentStorageLog)
    {
        $this->header['current_storage_log'] = $currentStorageLog;
        return $this;
    }

    /**
     * Get signed proof
     * @return string
     */
    public function getSignedProof()
    {
        return $this->header['signed_proof'];
    }

    /**
     * Set signed proof
     * @return LastLogModel
     */
    public function setSignedProof(string $signedProof)
    {
        $this->header['signed_proof'] = $signedProof;
        return $this;
    }

    /**
     * Get lastRawEntry
     * @return string
     */
    public function getLastRawEntry()
    {
        return $this->lastRawEntry;
    }

    /**
     * Set lastRawEntry
     * @return LastLogModel
     */
    public function setLastRawEntry(string $lastRawEntry)
    {
        $this->lastRawEntry = $lastRawEntry;
        return $this;
    }

    /**
     * Get lastEntry
     * @return LogModel
     */
    public function getLastEntry()
    {
        return $this->lastEntry;
    }

    /**
     * Set lastEntry
     * @return LastLogModel
     */
    public function setLastEntry(LogModel $lastEntry)
    {
        $this->lastEntry = $lastEntry;
        return $this;
    }
}
