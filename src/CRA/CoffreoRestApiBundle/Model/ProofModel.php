<?php
// CRA\CoffreoRestApiBundle/Model/ProofModel.php

namespace CRA\CoffreoRestApiBundle\Model;

use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Model\StorageLogModel;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

/**
 * ProofModel
 */
class ProofModel
{
    /**
     * fileId
     * @Groups({"proof"})
     * @var string
     */
    private $fileId;

    /**
     * Is proof valid
     * @Groups({"proof"})
     * @var bool
     */
    private $isValid;

    /**
     * Generation at
     * @Groups({"proof"})
     * @var string
     */
    private $generationAt;

    /**
     * Proof
     * @Groups({"proof"})
     * @var string
     */
    private $proof;

    /**
     * Array of logs
     * @Groups({"proof"})
     * @var array
     */
    private $trace;

    public function __construct(DataModel $data) {
        $this->fileId = $data->getFileId();
        $this->trace = [];
        $this->generationAt = (new \DateTime())->format("m-d-Y H:i:s");
    }

    /**
     * get id
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * set id
     * @return string
     */
    public function setFileId(int $fileId)
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * Get isValid
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * Get isValid
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * set isValid
     * @return bool
     */
    public function setValid(bool $valid)
    {
        $this->isValid = $valid;
        return $this;
    }

    /**
     * Get Proof
     * @return string
     */
    public function getProof()
    {
        return $this->proof;
    }

    /**
     * Set Proof
     * @return ProofModel
     */
    public function setProof(string $proof)
    {
        $this->proof = $proof;
        return $this;
    }

    /**
     * Get trace
     * @return array
     */
    public function getTrace()
    {
        return $this->trace;
    }

    /**
     * Add storage history to trace
     * @return ProofModel
     */
    public function appendStorageHistory(string $storagePath,
                                        array $storageHistory,
                                        StorageLogModel $storageLog)
    {
        $this->trace []= [
            "hash_algorithm" => $storageLog->getHashAlgorithm(),
            "symmetric_algorithm" => $storageLog->getSymmetricAlgorithm(),
            "storage" => $storagePath,
            "cnil" => $storageLog->getCnil(),
            "created_at" => $storageLog->getTimestamp(),
            "trace" => $storageHistory
        ];
        return $this;
    }
}
