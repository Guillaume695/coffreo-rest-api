<?php
// CRA\CoffreoRestApiBundle/Model/StorageLogModel.php
namespace CRA\CoffreoRestApiBundle\Model;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * StorageLogModel
 */
class StorageLogModel
{
    /**
     * Last log file header
     * @Groups({"log"})
     * @Type("array")
     * @var array
     */
    private $header;

    /**
     * Array of log entries
     * @Groups({"log"})
     * @Type("array")
     * @var array
     */
    private $rawEntries;

    public function __construct()
    {
        $this->header = [
            "timestamp" => "",
            "client_id" => "",
            "file_id" => "",
            "model" => "",
            "cnil" => 0,
            "hash_algorithm" => "",
            "symmetric_algorithm" => "",
            "previous_storage_log" => ""
        ];
        $this->rawEntries = [];
    }

    /**
     * Create a new StorageLogModel instance
     * @return StorageLogModel
     */
    public static function createNewStorageLog(DataModel $data) {
        $newStorageLog = new StorageLogModel();
        $newStorageLog->setTimestamp((new \DateTime())->getTimestamp());
        $newStorageLog->setClientId($data->getClient()->getClientId());
        $newStorageLog->setFileId($data->getFileId());
        if ($data->getCnil()) {
            $newStorageLog->setCnil(1);
        }
        return $newStorageLog;
    }

    /**
     * Get timestamp
     * @return integer
     */
    public function getTimestamp()
    {
        return $this->header['timestamp'];
    }

    /**
     * Set timestamp
     * @return StorageLogModel
     */
    public function setTimestamp(int $timestamp)
    {
        $this->header['timestamp'] = $timestamp;
        return $this;
    }

    /**
     * Get client id
     * @return integer
     */
    public function getClientId()
    {
        return $this->header['client_id'];
    }

    /**
     * Set client_id
     * @return StorageLogModel
     */
    public function setClientId(string $clientId)
    {
        $this->header['client_id'] = $clientId;
        return $this;
    }

    /**
     * Get file id
     * @return integer
     */
    public function getFileId()
    {
        return $this->header['file_id'];
    }

    /**
     * Set file_id
     * @return StorageLogModel
     */
    public function setFileId(string $fileId)
    {
        $this->header['file_id'] = $fileId;
        return $this;
    }

    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return $this->header['model'];
    }

    /**
     * Set model
     * @return StorageLogModel
     */
    public function setModel(string $model)
    {
        $this->header['model'] = $model;
        return $this;
    }

    /**
     * Get cnil
     * @return int
     */
    public function getCnil()
    {
        return $this->header['cnil'];
    }

    /**
     * Set cnil
     * @return StorageLogModel
     */
    public function setCnil(int $cnil)
    {
        $this->header['cnil'] = $cnil;
        return $this;
    }

    /**
     * Get hash algorithm
     * @return string
     */
    public function getHashAlgorithm()
    {
        return $this->header['hash_algorithm'];
    }

    /**
     * Set hash algorithm
     * @return StorageLogModel
     */
    public function setHashAlgorithm(string $hashAlgorithm)
    {
        $this->header['hash_algorithm'] = $hashAlgorithm;
        return $this;
    }

    /**
     * Get symmetric algorithm
     * @return string
     */
    public function getSymmetricAlgorithm()
    {
        return $this->header['symmetric_algorithm'];
    }

    /**
     * Set symmetric algorithm
     * @return StorageLogModel
     */
    public function setSymmetricAlgorithm(string $symmetricAlgorithm)
    {
        $this->header['symmetric_algorithm'] = $symmetricAlgorithm;
        return $this;
    }

    /**
     * Get previous storage log
     * @return string
     */
    public function getPreviousStorageLog()
    {
        return $this->header['previous_storage_log'];
    }

    /**
     * Set previous storage log
     * @return StorageLogModel
     */
    public function setPreviousStorageLog(string $previousStorageLog)
    {
        $this->header['previous_storage_log'] = $previousStorageLog;
        return $this;
    }

    /**
     * Get raw entries
     * @return array
     */
    public function getRawEntries()
    {
        return $this->rawEntries;
    }

    /**
     * Delete last raw entry
     * @return StorageLogModel
     */
    public function deleteLastRawEntry()
    {
        array_pop($this->rawEntries);
        return $this;
    }
}
