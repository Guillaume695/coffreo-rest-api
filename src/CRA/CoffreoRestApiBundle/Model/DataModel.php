<?php
// CRA\CoffreoRestApiBundle/Model/DataModel.php

namespace CRA\CoffreoRestApiBundle\Model;

use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Entity\File;
use CRA\CoffreoRestApiBundle\Model\LogModel;
use Symfony\Component\Validator\Constraints as Assert;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

/**
 * DataModel
 */
class DataModel
{
    /**
     * fileId
     * @Groups({"sysadmin", "user", "download"})
     * @Assert\Type("string")
     * @var string
     */
    private $fileId;

    /**
     * File Version
     * @Groups({"sysadmin", "user", "download"})
     * @Assert\Type("int")
     * @var int
     */
    private $version;

    /**
     * Nonce used for log & file encryption / decryption
     * @Assert\Type("string")
     * @var string
     */
    private $nonce;

    /**
     * Update type (should be 'md', 'doc', or 'all')
     * @Assert\Type("string")
     * @var string
     */
    private $updateType;

    /**
     * Download type (should be 'md', 'doc', or 'all')
     * @Assert\Type("string")
     * @var string
     */
    private $downloadType;

    /**
     * Initial file name
     * @Groups({"download"})
     * @Assert\Type("string")
     * @var string
     */
    private $initFileName;

    /**
     * Base64 raw file
     * @Groups({"download"})
     * @Assert\Type("string")
     * @var string
     */
    private $fileRawData;

    /**
     * File hash
     * @Assert\Type("string")
     * @var string
     */
    private $fileHash;

    /**
     * Previous File hash
     * @var string
     */
    private $previousFileHash;

    /**
     * Initial metadata name
     * @Groups({"download"})
     * @Assert\Type("string")
     * @var string
     */
    private $initMdName;

    /**
     * Raw metadata
     * @Groups({"download"})
     * @Assert\Type("string")
     * @var string
     */
    private $mdRawData;

    /**
     * Md hash
     * @Assert\Type("string")
     * @var string
     */
    private $mdHash;

    /**
     * Previous Md hash
     * @var string
     */
    private $previousMdHash;

    /**
     * CNIL file
     * @Assert\Type(type="int")
     * @var int
     */
    private $cnil;

    /**
     * Last Log URI
     * @Assert\Type("string")
     * @var string
     */
    private $lastLogDir;

    /**
     * Last Log filename
     * @Assert\Type("string")
     * @var string
     */
    private $lastLogName;

    /**
     * Storage log URI
     * @Assert\Type("string")
     * @var string
     */
    private $storageLogDir;

    /**
     * Storage log name
     * @Assert\Type("string")
     * @var string
     */
    private $storageLogName;

    /**
     * DataModel URI
     * @Assert\Type("string")
     * @var string
     */
    private $fileDir;

    /**
     * Filename
     * @Assert\Type("string")
     * @var string
     */
    private $fileName;

    /**
     * Metadata URI
     * @Assert\Type("string")
     * @var string
     */
    private $mdDir;

    /**
     * mdName
     * @Assert\Type("string")
     * @var string
     */
    private $mdName;

    /**
     * DataModel owner
     * @var Client
     */
    private $client;

    /**
     * Constructor
     * @return DataModel
     */
    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * Duplicate dataModel
     * @return DataModel
     */
    public static function duplicateDataModel(DataModel $oldData) {
        $newData = new DataModel($oldData->getClient());
        $newData->setCnil($oldData->getCnil());
        $newData->setFileId($oldData->getFileId());
        $newData->setFullFilePath($oldData->getFullFilePath());
        $newData->setFullLastLogPath($oldData->getFullLastLogPath());
        $newData->setFullMdPath($oldData->getFullMdPath());
        $newData->setInitFileName($oldData->getInitFileName());
        $newData->setInitMdName($oldData->getInitMdName());
        $newData->setVersion($oldData->getVersion());
        return $newData;
    }

    /**
     * Update dataModel with File object
     */
    public function updateDataWithFile(File $file) {
        $this->setFileId($file->getFileId());
        $this->setFullLastLogPath($file->getFullLastLogPath());
        $this->setFullStorageLogPath($file->getFullStorageLogPath());
        $this->setVersion($file->getVersion());
        $this->setNonce($file->getNonce());
    }

    /**
     * Update dataModel with last log Entry
     */
    public function updateDataWithLastEntry(LogModel $lastEntry) {
        $this->setFullFilePath($lastEntry->getFullFilePath());
        $this->setFullMdPath($lastEntry->getFullMdPath());
        $this->setPreviousFileHash($lastEntry->getFileHash());
        $this->setPreviousMdHash($lastEntry->getMdHash());
        $this->setInitFileName($lastEntry->getInitFileName());
        $this->setInitMdName($lastEntry->getInitMdName());
    }

    /**
     * get id
     * @return string
     */
    public function getFileId() {
        return $this->fileId;
    }

    /**
     * set id
     * @return DataModel
     */
    public function setFileId(string $fileId)
    {
        $this->fileId = $fileId;
        $prefix = substr($this->client->getClientId(), 0, CRASecurityService::RANDOM_ID_LENGTH);
        $this->lastLogDir = $prefix . '/';
        $this->storageLogDir = $prefix . '/storage/';
        $this->fileDir = $prefix . '/' . $this->fileId . '/doc/';
        $this->mdDir = $prefix . '/' . $this->fileId . '/md/';
        return $this;
    }

    /**
     * get version
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * set version
     * @return DataModel
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Increment version number
     * @return DataModel
     */
    public function incrementVersion() {
        $this->version = $this->version + 1;
        return $this;
    }

    /**
     * get nonce (HEX)
     * @return string
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * get nonce (BIN)
     * @return string
     */
    public function getNonceBIN()
    {
        return sodium_hex2bin($this->nonce);
    }

    /**
     * set nonce
     * @return DataModel
     */
    public function setNonce(string $nonce)
    {
        $this->nonce = $nonce;
        return $this;
    }

    /**
     * get updateType
     * @return string
     */
    public function getUpdateType()
    {
        return $this->updateType;
    }

    /**
     * set updateType
     * @return DataModel
     */
    public function setUpdateType(string $updateType)
    {
        $this->updateType = $updateType;
        return $this;
    }

    /**
     * Check that the correct inputs are not empty, following the update_type value
     * @Assert\IsTrue(message = "the update_type parameter of your request doesn't match the file & md inputs.")
     * @return bool
     */
    public function isUpdateTypeConsistent()
    {
        if ($this->updateType !== null) {
            switch($this->updateType) {
                case 'doc':
                    $doc = ((strlen($this->initFileName) > 0) && (strlen($this->fileRawData) > 0));
                    $md = ((strlen($this->initMdName) == 0) && (strlen($this->mdRawData) == 0));
                    return $md && $doc;
                case 'md':
                    $md = ((strlen($this->initMdName) > 0) && (strlen($this->mdRawData) > 0));
                    $doc = ((strlen($this->initFileName) == 0) && (strlen($this->fileRawData) == 0));
                    return $md && $doc;
                case 'all':
                    $md = ((strlen($this->initFileName) > 0) && (strlen($this->fileRawData) > 0));
                    $doc = ((strlen($this->initMdName) > 0) && (strlen($this->mdRawData) > 0));
                    return $md && $doc;
                default:
                    return false;
            }
        }
        return true;
    }

    /**
     * get downloadType
     * @return string
     */
    public function getDownloadType()
    {
        return $this->downloadType;
    }

    /**
     * set downloadType
     * @return DataModel
     */
    public function setDownloadType(string $downloadType)
    {
        $this->downloadType = $downloadType;
        return $this;
    }

    /**
     * get initFileName
     * @return string
     */
    public function getInitFileName()
    {
        return $this->initFileName;
    }

    /**
     * set initFileName
     * @return DataModel
     */
    public function setInitFileName(string $initFileName)
    {
        $this->initFileName = $initFileName;
        return $this;
    }

    /**
     * Reset initFileName
     * @return DataModel
     */
    public function resetInitFileName()
    {
        $this->initFileName = null;
        return $this;
    }

    /**
     * get fileRawData
     * @return string
     */
    public function getFileRawData() {
        return $this->fileRawData;
    }

    /**
     * set fileRawData
     * @return DataModel
     */
    public function setFileRawData(string $fileRawData)
    {
        $this->fileRawData = $fileRawData;
        return $this;
    }

    /**
     * get fileHash
     * @return string
     */
    public function getFileHash() {
        return $this->fileHash;
    }

    /**
     * set fileHash
     * @return DataModel
     */
    public function setFileHash(string $fileHash)
    {
        $this->fileHash = $fileHash;
        return $this;
    }

    /**
     * get previous fileHash
     * @return string
     */
    public function getPreviousFileHash()
    {
        return $this->previousFileHash;
    }

    /**
     * set previous fileHash
     * @return DataModel
     */
    public function setPreviousFileHash(string $previousFileHash)
    {
        $this->previousFileHash = $previousFileHash;
        return $this;
    }

    /**
     * get mdRawData
     * @return string
     */
    public function getMdRawData() {
        return $this->mdRawData;
    }

    /**
     * set mdRawData
     * @return DataModel
     */
    public function setMdRawData(string $mdRawData)
    {
        $this->mdRawData = $mdRawData;
        return $this;
    }

    /**
     * get mdHash
     * @return string
     */
    public function getMdHash()
    {
        return $this->mdHash;
    }

    /**
     * set mdHash
     * @return DataModel
     */
    public function setMdHash(string $mdHash)
    {
        $this->mdHash = $mdHash;
        return $this;
    }

    /**
     * get previous mdHash
     * @return string
     */
    public function getPreviousMdHash()
    {
        return $this->previousMdHash;
    }

    /**
     * set previous MdHash
     * @return DataModel
     */
    public function setPreviousMdHash(string $previousMdHash)
    {
        $this->previousMdHash = $previousMdHash;
        return $this;
    }

    /**
     * get initMdName
     * @return string
     */
    public function getInitMdName()
    {
        return $this->initMdName;
    }

    /**
     * set initMdName
     * @return DataModel
     */
    public function setInitMdName(string $initMdName)
    {
        $this->initMdName = $initMdName;
        return $this;
    }

    /**
     * Reset initMdName
     * @return DataModel
     */
    public function resetInitMdName()
    {
        $this->initMdName = null;
        return $this;
    }

    /**
     * get cnil
     * @return int
     */
    public function getCnil() {
        return $this->cnil;
    }

    /**
     * set cnil
     * @return DataModel
     */
    public function setCnil(int $cnil)
    {
        $this->cnil = $cnil;
        return $this;
    }

    /**
     * get lastLogDir
     * @return string
     */
    public function getLastLogDir() {
        return $this->lastLogDir;
    }

    /**
     * set lastLogDir
     * @return DataModel
     */
    public function setLastLogDir(string $lastLogDir)
    {
        $this->lastLogDir = $lastLogDir;
        return $this;
    }

    /**
     * get lastLogName
     * @return string
     */
    public function getLastLogName()
    {
        return $this->lastLogName;
    }

    /**
     * set lastLogName
     * @return DataModel
     */
    public function setLastLogName(string $lastLogName)
    {
        $this->lastLogName = $lastLogName;
        return $this;
    }

    /**
     * get fullLastLogPath
     * @return string
     */
    public function getFullLastLogPath()
    {
        return $this->lastLogDir . $this->lastLogName;
    }

    /**
     * set fullLastLogPath
     * @return DataModel
     */
    public function setFullLastLogPath(string $fullLastLogPath)
    {
        if (strlen($fullLastLogPath) > 0) {
            $splittedPath = explode('/', $fullLastLogPath);
            $this->setLastLogDir($splittedPath[0] . '/');
            $this->setLastLogName($splittedPath[1]);
        }
        return $this;
    }

    /**
     * Reset fullLastLogPath
     */
    public function resetFullLastLogPath() {
        $this->lastLogDir = "";
        $this->lastLogName = "";
        return $this;
    }

    /**
     * get storageLogDir
     * @return string
     */
    public function getStorageLogDir()
    {
        return $this->storageLogDir;
    }

    /**
     * set storageLogDir
     * @return DataModel
     */
    public function setStorageLogDir(string $storageLogDir)
    {
        $this->storageLogDir = $storageLogDir;
        return $this;
    }

    /**
     * get storageLogName
     * @return string
     */
    public function getStorageLogName()
    {
        return $this->storageLogName;
    }

    /**
     * set storageLogName
     * @return DataModel
     */
    public function setStorageLogName(string $storageLogName)
    {
        $this->storageLogName = $storageLogName;
        return $this;
    }

    /**
     * get fullStorageLogPath
     * @return string
     */
    public function getFullStorageLogPath()
    {
        return $this->storageLogDir.$this->storageLogName;
    }

    /**
     * set fullStorageLogPath
     * @return DataModel
     */
    public function setFullStorageLogPath(string $fullStorageLogPath)
    {
        if (strlen($fullStorageLogPath) > 0) {
            $splittedPath = explode('/storage/', $fullStorageLogPath);
            $this->setStorageLogDir($splittedPath[0] . '/storage/');
            $this->setStorageLogName($splittedPath[1]);
        }
        return $this;
    }

    /**
     * get fileDir
     * @return string
     */
    public function getFileDir() {
        return $this->fileDir;
    }

    /**
     * set fileDir
     * @return DataModel
     */
    public function setFileDir(string $fileDir)
    {
        $this->fileDir = $fileDir;
        return $this;
    }

    /**
     * get fileName
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * set fileName
     * @return DataModel
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * get fullFilePath
     * @return string
     */
    public function getFullFilePath()
    {
        return $this->fileDir.$this->fileName;
    }

    /**
     * set fullFilePath
     * @return DataModel
     */
    public function setFullFilePath(string $fullFilePath)
    {
        if (strlen($fullFilePath) > 0) {
            $splittedPath = explode('/doc/', $fullFilePath);
            $this->setFileDir($splittedPath[0] . '/doc/');
            $this->setFileName($splittedPath[1]);
        }
        return $this;
    }

    /**
     * get mdDir
     * @return string
     */
    public function getMdDir() {
        return $this->mdDir;
    }

    /**
     * set mdDir
     * @return DataModel
     */
    public function setMdDir(string $mdDir)
    {
        $this->mdDir = $mdDir;
        return $this;
    }

    /**
     * get mdName
     * @return string
     */
    public function getMdName()
    {
        return $this->mdName;
    }

    /**
     * set mdName
     * @return DataModel
     */
    public function setMdName(string $mdName)
    {
        $this->mdName = $mdName;
        return $this;
    }

    /**
     * get fullMdPath
     * @return string
     */
    public function getFullMdPath()
    {
        return $this->mdDir.$this->mdName;
    }

    /**
     * set fullMdPath
     * @return DataModel
     */
    public function setFullMdPath(string $fullMdPath)
    {
        if (strlen($fullMdPath) > 0) {
            $splittedPath = explode('/md/', $fullMdPath);
            $this->setMdDir($splittedPath[0] . '/md/');
            $this->setMdName($splittedPath[1]);
        }
        return $this;
    }

    /**
     * get client
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * set client
     * @return DataModel
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }
}
