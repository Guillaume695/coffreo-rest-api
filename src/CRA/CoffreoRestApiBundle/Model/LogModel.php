<?php
// CRA\CoffreoRestApiBundle/Model/LogModel.php
namespace CRA\CoffreoRestApiBundle\Model;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use FOS\OAuthServerBundle\Util\Random;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * LogModel
 */
class LogModel
{
    /**
     * Action
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $action;

    /**
     * File Version
     * @Groups({"log", "proof"})
     * @Type("int")
     * @var int
     */
    private $version;
    
    /**
     * Log timestamp
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var \DateTime
     */
    private $timestamp;

    /**
     * File id
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $fileId;

    /**
     * Client id
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $clientId;

    /**
     * File hash
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $fileHash;

    /**
     * Md hash
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $mdHash;

    /**
     * Previous signed proof (signed hash of the last log)
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $prevSignedProof;

    /**
     * File URI
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $fullFilePath;

    /**
     * Md URI
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $fullMdPath;

    /**
     * File Init name
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $initFileName;
    
    /**
     * Md Init name
     * @Groups({"log", "proof"})
     * @Type("string")
     * @var string
     */
    private $initMdName;

    /**
     * Constructor
     * @return FileModel
     */
    public function __construct(DataModel $data, string $action) {
        $this->fileId = $data->getFileId();
        $this->clientId = $data->getClient()->getClientId();
        $this->action = $action;
        $this->fileHash = $data->getFileHash();
        $this->mdHash = $data->getMdHash();
        $this->version = $data->getVersion();
        $this->fullFilePath = $data->getFullFilePath();
        $this->fullMdPath = $data->getFullMdPath();
        $this->initFileName = $data->getInitFileName();
        $this->initMdName = $data->getInitMdName();
        $this->timestamp = (new \DateTime())->getTimestamp();
    }

    /**
     * get version
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * get fileId
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * get clientId
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * get fileHash
     * @return string
     */
    public function getFileHash()
    {
        return $this->fileHash;
    }

    /**
     * get mdHash
     * @return string
     */
    public function getMdHash()
    {
        return $this->mdHash;
    }

    /**
     * get action
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * get prevSignedProof
     * @return string
     */
    public function getPrevSignedProof() {
        return $this->prevSignedProof;
    }

    /**
     * get fullFilePath
     * @return string
     */
    public function getFullFilePath() {
        return $this->fullFilePath;
    }

    /**
     * get fullMdPath
     * @return string
     */
    public function getFullMdPath() {
        return $this->fullMdPath;
    }

    /**
     * get initFileName
     * @return string
     */
    public function getInitFileName() {
        return $this->initFileName;
    }

    /**
     * get initMdName
     * @return string
     */
    public function getInitMdName() {
        return $this->initMdName;
    }

    /**
     * get timestamp
     * @return \DateTime
     */
    public function getTimestamp() {
        return $this->timestamp;
    }

    /**
     * set version
     * @return Log
     */
    public function setVersion (int $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * set fileId
     * @return Log
     */
    public function setFileId (string $fileId)
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * set clientId
     * @return Log
     */
    public function setClientId(string $clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * set fileHash
     * @return Log
     */
    public function setFileHash(string $fileHash)
    {
        $this->fileHash = $fileHash;
        return $this;
    }

    /**
     * set mdHash
     * @return Log
     */
    public function setMdHash (string $mdHash)
    {
        $this->mdHash = $mdHash;
        return $this;
    }

    /**
     * set action
     * @return Log
     */
    public function setAction (string $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * set prevSignedProof
     * @return Log
     */
    public function setPrevSignedProof (string $prevSignedProof)
    {
        $this->prevSignedProof = $prevSignedProof;
        return $this;
    }

    /**
     * set fullFilePath
     * @return Log
     */
    public function setFullFilePath (string $fullFilePath)
    {
        $this->fullFilePath = $fullFilePath;
        return $this;
    }

    /**
     * set fullMdPath
     * @return Log
     */
    public function setFullMdPath (string $fullMdPath)
    {
        $this->fullMdPath = $fullMdPath;
        return $this;
    }

    /**
     * set initFileName
     * @return Log
     */
    public function setInitFileName (string $initFileName)
    {
        $this->initFileName = $initFileName;
        return $this;
    }

    /**
     * set initMdName
     * @return Log
     */
    public function setInitMdName (string $initMdName)
    {
        $this->initMdName = $initMdName;
        return $this;
    }

    /**
     * set timestamp
     * @return Log
     */
    public function setTimestamp(\DateTime $timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }
}
