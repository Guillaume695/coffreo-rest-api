<?php

namespace tests\CoffreoRestApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use CRA\CoffreoRestApiBundle\SecurityService\CRAFileService;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use FOS\OAuthServerBundle\Util\Random;

class FileServiceControllerTest extends KernelTestCase
{
    private $fileService;
    private $user;
    private $client;
    private $randomId;
    private $secret;
    private $nonce;
    private $fileId;

    protected function setUp()
    {
        self::bootKernel();

        $this->fileService = static::$kernel
            ->getContainer()
            ->get('cra_coffreo.FileService');

        $this->user = new User("");
        $this->user->setFileUserKey("5b08c9de501f243e700607fa5df7e7c37e036e1ac3019d0ed9bf4d6ed077dcf7");

        $this->client = new Client();
        $this->client->setUser($this->user);

        $this->randomId = "1234567890";
        $this->secret = "1234567890";
        $this->nonce = "123456789012abcdefabcdef";
        $this->fileId = "1234567890";
        
        $this->client->setRandomId($this->randomId);
        $this->client->setSecret($this->secret);
        $this->client->setAllowedGrantTypes(array('client_credentials'));
    }

    public function testEncryptAndWriteFile() {
        $data = new DataModel($this->client);
        $data->setFileId($this->fileId);
        $data->setNonce($this->nonce);
        $data->setFileRawData("RFRZQ29mZnJlbzIwMTc=");

        $this->fileService->encryptAndWriteFile($data, $this->randomId);
        $this->assertTrue(is_file($this->fileService->getTargetDirFile() . "1234567890/doc/doc_1234567890"));
    }

    public function testReadAndDecryptFile() {
        $data = new DataModel($this->client);
        $data->setNonce($this->nonce);
        $data->setFullFilePath("1234567890/doc/doc_1234567890");

        $this->fileService->readAndDecryptFile64($data);

        $this->assertEquals($data->getFileRawData(), "RFRZQ29mZnJlbzIwMTc=");
    }

    public function testEncryptAndWriteMd() {
        $data = new DataModel($this->client);
        $data->setFileId($this->fileId);
        $data->setNonce($this->nonce);
        $data->setMdRawData("RFRZQ29mZnJlbzIwMTc=");

        $this->fileService->encryptAndWriteMd($data, $this->randomId);
        $this->assertTrue(is_file($this->fileService->getTargetDirFile() . "1234567890/md/md_1234567890"));
    }

    public function testReadAndDecryptMd() {
        $data = new DataModel($this->client);
        $data->setNonce($this->nonce);
        $data->setFullMdPath("1234567890/md/md_1234567890");

        $this->fileService->readAndDecryptMd64($data);

        $this->assertEquals($data->getMdRawData(), "RFRZQ29mZnJlbzIwMTc=");
    }

    public function testDeleteVersion() {
        $data = new DataModel($this->client);
        $data->setNonce($this->nonce);
        $data->setFullFilePath("1234567890/doc/doc_1234567890");
        $data->setFullMdPath("1234567890/md/md_1234567890");

        $this->fileService->deleteVersion($data);

        $this->assertTrue(!is_file($this->fileService->getTargetDirFile() . "1234567890/doc/doc_1234567890"));
        $this->assertTrue(!is_file($this->fileService->getTargetDirFile() . "1234567890/md/md_1234567890"));
    }
}
