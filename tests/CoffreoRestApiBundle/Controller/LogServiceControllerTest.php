<?php

namespace tests\CoffreoRestApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use CRA\CoffreoRestApiBundle\SecurityService\CRALogService;
use CRA\CoffreoRestApiBundle\Model\DataModel;
use CRA\OAuthServerBundle\Entity\Client;
use CRA\CoffreoRestApiBundle\Entity\User;
use FOS\OAuthServerBundle\Util\Random;

class LogServiceControllerTest extends KernelTestCase
{
    private $logService;
    private $user;
    private $client;
    private $randomId;
    private $secret;
    private $nonce;
    private $fileId;

    protected function setUp()
    {
        self::bootKernel();

        $this->logService = static::$kernel
            ->getContainer()
            ->get('cra_coffreo.LogService');

            $this->user = new User("");
            $this->user->setLogUserKey("5b08c9de501f243e700607fa5df7e7c37e036e1ac3019d0ed9bf4d6ed077dcf7");
    
            $this->client = new Client();
            $this->client->setClientId("1234567890");
            $this->client->setUser($this->user);

            $this->randomId = "1234567890";
            $this->secret = "1234567890";
            $this->nonce = "123456789012345678901234";
            $this->fileId = "1234567890";

            $this->client->setRandomId($this->randomId);
            $this->client->setSecret($this->secret);
            $this->client->setAllowedGrantTypes(array('client_credentials'));
    }

    public function testLog() {
        $data = new DataModel($this->client);
        $data->setFileId($this->fileId);
        $data->setNonce($this->nonce);
        $data->setFileRawData("RFRZQ29mZnJlbzIwMTc=");
        $data->setMdRawData("RFRZQ29mZnJlbzIwMTc=");

        $this->logService->appendLog($data, "Test");
        $randomLogId = explode('.', explode('-', $data->getLastLogName())[3])[0];

        $this->assertTrue(is_file($this->logService->getFuncLogPath() . "1234567890/last-log-1234567890-" . $randomLogId . ".json"));

        $lastLog = $this->logService->readAndDecryptLastLog($data);
        $this->assertEquals($lastLog->getLastEntry()->getAction(), "Test");
        
        $this->logService->deleteLogs($data);
        $this->assertTrue(!is_file($this->logService->getFuncLogPath() . "1234567890/last-log-1234567890-" . $randomLogId . ".json"));
    }
}
