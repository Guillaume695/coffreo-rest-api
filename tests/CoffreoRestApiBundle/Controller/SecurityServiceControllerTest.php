<?php

namespace tests\CoffreoRestApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use CRA\CoffreoRestApiBundle\SecurityService\CRASecurityService;

class SecurityServiceControllerTest extends KernelTestCase
{
    private $securityService;

    protected function setUp()
    {
        self::bootKernel();

        $this->securityService = static::$kernel
            ->getContainer()
            ->get('cra_coffreo.SecurityService');
    }

    public function testVariables() {
        $hash_algorithm = $this->securityService->getHashAlgorithm();
        $symetric_algorithm = $this->securityService->getSymmetricAlgorithm();

        $this->assertEquals($hash_algorithm, 'sha256');
        $this->assertEquals($symetric_algorithm, 'aes256-GCM');
    }

    public function testGenerateHash() {
        $data = "DTYCoffreo2017";
        $hash = $this->securityService->generateHash($data);
        $this->assertEquals($hash, '48cb1aef11761b9150eb8b590d09093af9031f1a24bfde21ba8a9be3df70ab64');
    }

    public function testGenerateEncryptedHash() {
        $data = "DTYCoffreo2017";
        $cypher_data = $this->securityService->generateEncryptedHash($data);
        $this->assertEquals($cypher_data, '447e3f2722a8f4cdc80a3d46ef282bde71e79083b7dc7fc0e6df699758400284a5ebd5bc2bed2073651cd473d9f34b45d62b66a40b9abd29478a79cb52ac8622e09370ed5fb71872689c302b2ad058b4');
    }

    public function testGenerateSymmetricKey() {
        $key = $this->securityService->generateSymmetricKey();
        $this->assertEquals(strlen($key), 64);
    }

    public function testGenerateSymmetricKeyBIN() {
        $key = $this->securityService->generateSymmetricKeyBIN();
        $this->assertEquals(strlen($key), 32);
    }

    public function testGenerateNonce() {
        $nonce = $this->securityService->generateNonce();
        $this->assertEquals(strlen($nonce), 24);
    }

    public function testGenerateRandomId() {
        $id = $this->securityService->generateRandomId();
        $this->assertEquals(strlen($id), 2*CRASecurityService::RANDOM_ID_LENGTH);
    }

    public function testSymmetricEncrypt() {
        $data = "DTYCoffreo2017";
        $key = "12345678901234567890123456789012";
        $nonce = "123456789012";
        $cypher_data = $this->securityService->symmetricEncrypt($data, $nonce, $key);

        $this->assertEquals($cypher_data, "c4a6a4ad6d27a793807d0db4e0221c31ba9211516ceef0aada97e8664686");
    }

    public function testSymmetricEncryptBIN() {
        $data = "DTYCoffreo2017";
        $key = "12345678901234567890123456789012";
        $nonce = "123456789012";
        $cypher_data = $this->securityService->symmetricEncryptBIN($data, $nonce, $key);

        $this->assertEquals($cypher_data, hex2bin("c4a6a4ad6d27a793807d0db4e0221c31ba9211516ceef0aada97e8664686"));
    }

    public function testSymmetricDecrypt() {
        $cypher_data = hex2bin("c4a6a4ad6d27a793807d0db4e0221c31ba9211516ceef0aada97e8664686");
        $key = "12345678901234567890123456789012";
        $nonce = "123456789012";
        $data = $this->securityService->symmetricDecrypt($cypher_data, $nonce, $key);

        $this->assertEquals($data, "DTYCoffreo2017");
    }

    public function testAsymmetricEncrypt() {
        $data = "DTYCoffreo2017";
        $cypher_data = $this->securityService->asymmetricEncrypt($data);

        $this->assertEquals($cypher_data, 'aa030ae8c855f3361553a82ab3e075a7018baaa2e9db7cd4b2816c915815');
    }

    public function testAsymmetricEncryptBIN() {
        $data = "DTYCoffreo2017";
        $cypher_data = $this->securityService->asymmetricEncryptBIN($data);

        $this->assertEquals($cypher_data, hex2bin('aa030ae8c855f3361553a82ab3e075a7018baaa2e9db7cd4b2816c915815'));
    }

    public function testAsymmetricDecrypt() {
        $cypher_data = hex2bin('aa030ae8c855f3361553a82ab3e075a7018baaa2e9db7cd4b2816c915815');
        $data = $this->securityService->asymmetricDecrypt($cypher_data);

        $this->assertEquals($data, "DTYCoffreo2017");
    }

    public function testSign() {
        $data = "DTYCoffreo2017";
        $signed_data = $this->securityService->sign($data);

        $this->assertEquals($signed_data, hex2bin("d69997b22da08cd9d22e9a8796b19e4781bab9df8d38018cab1b5bc70e489d558986ea39263c9422e9b057e960c12e280853291ebba0fdf66df951f3906faf06445459436f666672656f32303137"));
    }

    public function testSign_open() {
        $signed_data = hex2bin("d69997b22da08cd9d22e9a8796b19e4781bab9df8d38018cab1b5bc70e489d558986ea39263c9422e9b057e960c12e280853291ebba0fdf66df951f3906faf06445459436f666672656f32303137");
        $data = $this->securityService->sign_open($signed_data);

        $this->assertEquals($data, "DTYCoffreo2017");
    }
}
